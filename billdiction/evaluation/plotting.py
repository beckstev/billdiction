import numpy as np
import os
import matplotlib.pyplot as plt

from matplotlib.ticker import MaxNLocator
from matplotlib.patches import Polygon, Rectangle
from pathlib import Path

import torchvision.transforms.functional as F

# BillDiction Library
import billdiction.net.utils as utils
from billdiction.io.dataset import get_dataset
from billdiction.net.inference import CornerNN
from billdiction.io.data_manipulation import sort_corners
from billdiction.io.data_augmentation import ComposeLabel, NormalizeLabel, ToTensorLabel, ResizeLabel


def plot_learning_curve(monitor_df, save_path, ylog=False):

    fig, ax = plt.subplots(figsize=(12.8, 7.2))

    # plot training loss evolution
    ax.plot(monitor_df.index, monitor_df['train_loss_mean'], '-', color='blue',
            label='Training Loss')

    # plot validation loss evolution
    ax.plot(monitor_df.index, monitor_df['val_loss_mean'], '--', color='orange',
            label='Validation Loss')

    # legend
    ax.set_title('Learning Curve')

    ax.set_xlabel('Epochs')
    ax.set_ylabel('MSE Loss')

    if ylog:
        ax.set_yscale('log')

    ax.xaxis.set_major_locator(MaxNLocator(integer=True))

    ax.legend()

    fig.savefig(save_path)


def visualize_predictions(model_path, save_images=False, show_mean=False,
                          print_prediction=False, shuffle=False):

    # read model's training config
    training_config = utils.load_config(model_path)

    # collect relevant variables from parameters
    architecture = training_config['architecture']
    use_rgb = training_config['use_rgb']
    training_type = training_config['training_type']
    original_size = training_config['original_image_size']
    crop_size = training_config['data_augmentation']['crop']['size']

    if save_images:
        img_save_path = os.path.join(model_path, 'img_eval')
        if not os.path.isdir(img_save_path):
            os.mkdir(img_save_path)

    # load data
    # normalize inputs
    transforms = ComposeLabel([
        ResizeLabel(size=crop_size),
        ToTensorLabel(),
        NormalizeLabel(mean=training_config['normalization']['mean'],
                       std=training_config['normalization']['std'])
    ])


    path_to_data = os.path.join(Path(os.path.abspath(__file__)).parents[2],
                                'bills')

    test_data = get_dataset(path_to_data, usage='test',
                            transforms=transforms,
                            use_rgb=use_rgb,
                            training_type=training_type)
    visual_data = get_dataset(path_to_data, usage='test',
                              transforms=ResizeLabel(size=crop_size),
                              use_rgb=use_rgb,
                              training_type=training_type)

    if show_mean:  # functionality to make sure that networks learned more than distribution means
        # load training data

        train_data = get_dataset(path_to_data, usage='train',
                                 transforms=transforms,
                                 use_rgb=use_rgb,
                                 training_type=training_type)

        # calculate corner position means
        x_coord = train_data.data.loc[:, ['x1', 'x2', 'x3', 'x4']]
        y_coord = train_data.data.loc[:, ['y1', 'y2', 'y3', 'y4']]

        x_scaled = x_coord.values / train_data.data.loc[:, 'width'].values.reshape(-1, 1) * train_data.image_width
        y_scaled = y_coord.values / train_data.data.loc[:, 'height'].values.reshape(-1, 1) * train_data.image_height

        x_means = np.mean(x_scaled, axis=0)
        y_means = np.mean(y_scaled, axis=0)

    # load model
    net = CornerNN(model_path)

    # set (shuffled) indices to loop over
    loop_indices = np.arange(len(test_data))
    if shuffle:
        loop_indices = np.random.permutation(loop_indices)

    for i in loop_indices:
        print(f"evaluating image {test_data.data.loc[i, 'filename']}")

        # get predictions and labels
        image, label = test_data[i]
        image, label = image.to(net.device), label.to(net.device)

        x_true, y_true, x_pred, y_pred = utils.predict_on_index(net=net, image=image, label=label)

        # define the figure
        fig, ax = plt.subplots(figsize=(5, 10))

        # plot ground truth
        ind = np.array([0, 1, 3, 2]) # changes sequence of coordinates for plotting
        xy_true = np.array([x_true, y_true]).T[ind]
        gt_box = Polygon(xy=xy_true,
                         color='g',
                         fill=False,
                         linewidth=2,
                         label='Ground Truth')
        ax.add_patch(gt_box)

        if print_prediction:
            print('Ground Truth:\n', x_true, y_true)

        # plot predictions
        xy_pred = np.array([x_pred, y_pred]).T[ind]
        pred_box = Polygon(xy=xy_pred,
                           color='r',
                           fill=False,
                           linewidth=2,
                           label='Prediction')
        ax.add_patch(pred_box)

        if print_prediction:
            print('Prediction:\n', x_pred, y_pred, '\n')

        # plot coordinate means
        if show_mean:
            ax.plot(x_means, y_means, 'cP', markersize=10, markeredgewidth=2,
                    label='Mean of Corner in Test Set')

        # plot the image
        image, label = visual_data[i]
        image = F.to_pil_image(image)
        ax.imshow(image, cmap='gray')

        ax.legend(loc='upper center', bbox_to_anchor=(0.5, 1.1))

        if save_images:
            fig.savefig(os.path.join(img_save_path, 'test_img{}'.format(i)))
            plt.close(fig)
            continue

        # use a fixed plotting window position for more convenient viewing of bills
        thismanager = plt.get_current_fig_manager()
        thismanager.window.setGeometry(50, 50, 1000, 1000)

        plt.show()


def bbox_plot(bboxes):
    """
    Visualize correlations between bounding boxes to study character segmentation from noise

    :param bboxes: np.ndarray of shape (N, 4), where n is the number of bounding boxes, whose shape is (x, y, w, h)
    :return: matplotlib figure
    """
    if len(bboxes) > 0:
        widths = bboxes[2:, 2]
        heights = bboxes[2:, 3]

        fig, ax = plt.subplots(1, 1, figsize=(10, 10))
        fig.tight_layout()

        ax.set_aspect(1)
        ax.scatter(x=widths, y=heights, alpha=0.5)

    else:
        fig, ax = plt.subplots(1, 1, figsize=(10, 10))
        fig.tight_layout()

    return fig


def visualize_detection_pipeline(detection_pipe):
    """
    Draw intermediate steps of the Billdiction Pipeline:
    * Corner prediction
    * Segmentation
    * OCR

    :param detection_pipe: Billdiction detection pipeline containing all intermediate results
    :return: Matplotlib figure
    """
    fig, (ax0, ax1, ax2, ax3) = plt.subplots(1, 4, figsize=(20, 10))
    fig.tight_layout()

    # input image
    ax0.imshow(detection_pipe['images']['input'], cmap="gray")
    corners = detection_pipe['corners']
    corners = corners[[0, 1, 3, 2]]
    ax0.plot(corners[:, 0], corners[:, 1], "r.", markersize=20)
    bbox = Polygon(corners, fill=False, color="g", linewidth=3, closed=True)
    ax0.add_patch(bbox)
    ax0.set_title('Grey scaled image and predicted corners')

    # character detection
    ax1.imshow(detection_pipe['images']['top-down-view'], cmap="gray")
    ax1.set_title('Canny character detection')
    for box in detection_pipe['characters']:
        draw_bbox(ax1, box, color="r")

    # text segmentation
    color_dict = {'total': 'C1',
                  'payment': 'C1',
                  'change': 'C1',
                  'text': 'r',
                  'number': 'r',
                  'product': 'g',
                  'price': 'g',
                  'location': 'm',
                  'time': 'm',
                  'date': 'm',
                  'logo': 'm',
                  'barcode': 'm',
                  'sum': 'C1',
                  'other': '#808080'}

    ax2.imshow(detection_pipe['images']['top-down-view'], cmap='gray')
    ax2.set_title('Text boxes')

    for _, row in detection_pipe['detections'].iterrows():
        box = row[['x', 'y', 'w', 'h']]
        label = row['label']
        draw_bbox(ax2, box, color="r")

        if row['corresponding']:
            corr = detection_pipe['detections'].loc[row['corresponding'], ['x', 'y', 'w', 'h']]
            product_x, product_y = box[0] + box[2], box[1] + box[3] / 2
            price_x, price_y = corr[0], corr[1] + corr[3] / 2
            ax3.plot([price_x, product_x], [price_y, product_y], 'r--', alpha=0.7)

        if label != 'entity':
            draw_bbox(ax3, box, color=color_dict[label], label=label)

    # final result
    ax3.imshow(detection_pipe['images']['top-down-view'], cmap='gray')
    height, width = detection_pipe['images']['top-down-view'].shape
    ax3.set_title('Detection Result')

    for l, g in detection_pipe['detections'].groupby('line'):
        y = (g['y'] + (g['h'] / 2)).mean()
        ax3.text(x=width + 100, y=y, s=str(l), size=12, ha='right', va="center")

    for local_area_segmentation in detection_pipe['area_edges']:
        ax3.axhline(local_area_segmentation['hcut'])

        if local_area_segmentation['vcut']:
            vcut = local_area_segmentation['vcut']
            ax3.axvline(vcut['delta_x'], ymin=vcut['ymin_vline'], ymax=vcut['ymax_vline'])

    return fig


def display_roi_text(detection_pipe):
    if detection_pipe['roi_text']:
        for roi, roi_text in zip(detection_pipe['roi'], detection_pipe['roi_text']):
            fig, ax = plt.subplots(1, 1, figsize=(10, 10))
            fig.tight_layout()

            ax.imshow(roi, cmap='gray')
            ax_pos = ax.get_position()
            ax.text(0.5, ax_pos.y1 + 0.1, f"Detection: {roi_text}", fontsize=18,
                    ha='center', va='bottom', transform=fig.transFigure)
            ax.axis('off')

            plt.show()


def draw_bbox(axis, logo_box, color, horizontal_alignment='left', label=None):
    logo_rect = Rectangle(xy=logo_box[:2], width=logo_box[2], height=logo_box[3], fill=False, color=color, linewidth=1.5)
    axis.add_patch(logo_rect)

    if label:
        axis.text(x=logo_box[0] + logo_box[2], y=logo_box[1],
                  s=label, size=10, ha=horizontal_alignment, va="top",
                  bbox={"boxstyle": "square", "fill": True, "pad": 0, "color": color, "alpha": 0.5})
