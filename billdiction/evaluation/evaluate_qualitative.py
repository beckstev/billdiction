import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import Ellipse

corner_names = ["upper left corner", "upper right corner", "lower left corner", "lower right corner"]


def scatter_distribution(x_true, y_true, x_pred, y_pred,
                         save_path=None, architecture="Unknown Architecture", quantitative=None):
    fig, ax = plt.subplots(1, 1, figsize=(5, 10))

    # plot corners
    alpha = 0.2
    ms = 10
    ax.scatter(x_true, y_true, c="g", alpha=alpha, label="Ground Truth")
    ax.scatter(x_pred, y_pred, c="r", alpha=alpha, label="Prediction")

    # axis options
    ax.set_aspect(1)
    ax.legend()
    ax.set_title(f"Distribution of Predicted and True Corner Positions ({architecture})")

    if save_path:
        fig.savefig(save_path)

    return fig


def histograms_distance_angle(x_true, y_true, x_pred, y_pred,
                              save_path=None, architecture="Unknown Architecture", quantitative=None):
    fig, axes = plt.subplots(2, 4, figsize=(16, 8), sharey='row')
    fig.suptitle(f"Distribution of Prediction Errors ({architecture})", fontsize=20)

    rect = (0.05, 0.2, 0.95, 0.9)
    fig.tight_layout(rect=rect)

    # plot distances
    distances = np.sqrt((x_pred - x_true) ** 2 + (y_pred - y_true) ** 2)
    for i, axis in enumerate(axes[0]):

        mean = np.round(quantitative['distances']['mean'][i], 1)
        std = np.round(quantitative['distances']['std'][i], 1)

        axis.hist(distances[:, i], bins=np.arange(20) * 5)
        axis.set(xlabel='distance', ylabel='counts')

        axis.axvline(mean, c='C1', alpha=0.4)
        axis.fill_between(x=[mean-std, mean+std], y1=axis.get_ylim()[1],
                          y2=0, color='C1', alpha=0.2)

        axis.set_title(corner_names[i])

    # plot angles
    for i, axis in enumerate(axes[1]):
        xy_true = np.stack([x_true[:, i], y_true[:, i]]).T
        xy_pred = np.stack([x_pred[:, i], y_pred[:, i]]).T

        r = xy_pred - xy_true
        angles = np.arctan2(r[:, 0], r[:, 1]) * 180 / np.pi

        axis.hist(angles, bins=np.arange(20) * 18 - 180)
        axis.set(xlabel='relative angle', ylabel='counts')
        axis.set_xticks([-180, -90, 0, 90, 180])

    # plot quantative numbers
    if quantitative:
        for i in range(len(axes[0])):
            for j, key in enumerate(quantitative.keys()):
                x_pos = rect[0] + (rect[2] - rect[0]) / 16 + i * (rect[2] - rect[0]) / 4
                y_pos = rect[1] * 0.7 - j * 0.04

                mean = np.round(quantitative[key]['mean'][i], 1)
                std = np.round(quantitative[key]['std'][i], 1)

                fig.text(x_pos, y_pos, s=f"{key}: {mean:.1f} $\pm$ {std:.1f}",
                         fontsize=12, horizontalalignment='left')

    if save_path:
        fig.savefig(save_path)

    return fig


def corner_offset(x_true, y_true, x_pred, y_pred,
                  save_path=None, architecture="Unknown Architecture", quantitative=None):
    fig, axes = plt.subplots(2, 2, figsize=(10, 10), sharex=True, sharey=True)
    fig.suptitle(f"Prediction Offset to Ground Truth ({architecture})", fontsize=20)

    # calculate offset
    x_offset = x_pred - x_true
    y_offset = y_pred - y_true

    x_max = np.abs(x_offset).max(axis=(0, 1))
    y_max = np.abs(x_offset).max(axis=(0, 1))

    # set fixed limits for comparision between models
    xlim = [-50, 50]  # 1.2 * np.array([-x_max, x_max])
    ylim = [50, -50]  # 1.2 * np.array([-y_max, y_max])

    mean_x_offset = np.mean(x_offset, axis=0)
    mean_y_offset = np.mean(y_offset, axis=0)

    std_x_offset = np.std(x_offset, axis=0, ddof=1)
    std_y_offset = np.std(y_offset, axis=0, ddof=1)

    # plot offset
    alpha = 0.8
    for i, axis in enumerate(axes.flatten()):

        ellipse = Ellipse(xy=(mean_x_offset[i], mean_y_offset[i]),
                          width=2*std_x_offset[i], height=2*std_y_offset[i],
                          color='C1', alpha=0.3)

        axis.scatter(x_offset[:, i], y_offset[:, i], alpha=alpha)
        axis.scatter(mean_x_offset[i], mean_y_offset[i], color='C1',
                     alpha=alpha)
        axis.add_patch(ellipse)
        axis.plot([0, 0], ylim, "k-", alpha=0.3)
        axis.plot(xlim, [0, 0], "k-", alpha=0.3)

        axis.set(title=corner_names[i], xlabel='width offset',
                 ylabel='height offset', xlim=xlim, ylim=ylim, aspect=1)

    if save_path:
        fig.savefig(save_path)

    return fig
