import numpy as np

def distance_angles_mean_std(x_true, y_true, x_pred, y_pred):
    xy_true = np.stack([x_true, y_true])
    xy_pred = np.stack([x_pred, y_pred])
    xy_rel = xy_pred - xy_true

    dist = np.linalg.norm(xy_rel, axis=0)
    dist_mean = np.mean(dist, axis=0)
    dist_std = np.std(dist, axis=0, ddof=1)

    angles = np.arctan2(xy_rel[1], xy_rel[0]) * 180 / np.pi
    angles_mean = np.mean(angles, axis=0)
    angles_std = np.std(angles, axis=0, ddof=1)

    return dist_mean, dist_std, angles_mean, angles_std