import pytest

from billdiction.net.losses import IoULoss


def test_IoULoss():
    input = [0.2, 0.8, 0, 1.8]
    target = [0, 1, 0, 2]
    expected_loss = 0.6 * 1.8 / 2

    loss = IoULoss()

    assert loss(input, target) - expected_loss < 1e-10
