import torch
import torch.nn as nn


class SmoothMeanDistanceLoss(nn.Module):
    """
    Returns the smooth mean of distances between pairs (x, y) of points in a label tensor of the form (x_1, y_1, ..., x_n, y_n).
    """
    def __init__(self, reduction='mean'):
        super(SmoothMeanDistanceLoss, self).__init__()
        self.reduction = reduction

    def forward(self, input, target):
        assert len(input.shape) == 2, f"Loss functions input does not have the right shape. Shape is {input.shape}"
        diff = (input - target) ** 2
        distances = diff[:, ::2] + diff[:, 1::2]
        smooth_distance = torch.where(distances < 1, 0.5 * distances, torch.sqrt(distances) - 0.5)
        mean_distance = smooth_distance.mean()
        return mean_distance


class MeanSquaredDistanceLoss(nn.Module):
    """
    Returns the mean of squared distances between pairs (x, y) of points in a label tensor of the form (x_1, y_1, ..., x_n, y_n).
    """
    def __init__(self, reduction='mean'):
        super(MeanSquaredDistanceLoss, self).__init__()
        self.reduction = reduction

    def forward(self, input, target):
        assert len(input.shape) == 2, f"Loss functions input does not have the right shape. Shape is {input.shape}"
        diff = (input - target) ** 2
        distances = diff[:, ::2] + diff[:, 1::2]
        mean_distance = distances.mean()
        return mean_distance


class IoULoss(nn.Module):
    def __init__(self):
        super(IoULoss, self).__init__()

    def forward(self, input, target):
        W_input, H_input = input[:, 1] - input[:, 0], input[:, 3] - input[:, 2]
        W_target, H_target = target[:, 1] - target[:, 0], target[:, 3] - target[:, 2]

        W_intersection = torch.min(input[:, 1], target[:, 1]) - torch.max(input[:, 0], target[:, 0])
        H_intersection = torch.min(input[:, 3], target[:, 3]) - torch.max(input[:, 2], target[:, 2])

        A_input = W_input * H_input
        A_target = W_target * H_target
        A_intersection = W_intersection * H_intersection

        A_union = A_input + A_target - A_intersection

        return A_intersection / A_union


class FasterRCNNLoss(nn.Module):
    def __init__(self, N_reg, N_cls, l=1, device='cpu'):
        super(FasterRCNNLoss, self).__init__()
        self.device = device

        self.iouloss = IoULoss()
        self.nllloss = nn.NLLLoss(reduction='none')
        self.l1loss = nn.SmoothL1Loss(reduction='none')

        self.N_reg = N_reg
        self.N_cls = N_cls
        self.l = l

    def forward(self, input, target):
        cls, reg = input
        loss = torch.Tensor([0]).to(self.device)

        # for single targets
        for i in range(reg.shape[-2]):
            for j in range(reg.shape[-1]):
                iou_loss = self.iouloss(reg[:, :, i, j], target)
                cls_label = (iou_loss > 0.7).long().to(self.device)
                cls_loss = self.nllloss(cls[:, :, i, j], cls_label)
                reg_loss = cls_label.float().view(-1, 1) * self.l1loss(reg[:, :, i, j], target)
                reg_loss = torch.sum(reg_loss, dim=1)

                loss += torch.sum((1/self.N_cls) * cls_loss + (1/self.N_reg) * reg_loss)

        return loss

