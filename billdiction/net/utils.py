import os
import json
import torch
import torch.nn as nn

# BillDiction Library
from billdiction.net import networks


def load_model(model_path, architecture, evaluation=False):
    model_path = os.path.join(model_path, '{}.state'.format(architecture))
    device = get_device()

    if device.type == 'cpu':
        state_dict = torch.load(model_path, map_location = 'cpu')
    else:
        state_dict = torch.load(model_path)

    net = getattr(networks, architecture)()
    net.load_state_dict(state_dict)

    net.to(device)

    if evaluation:
        net.eval()
    else:
        net.train()

    return net


def get_device():
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    return device


def load_config(model_path):
    model_config = os.path.join(model_path, 'training_config.json')
    with open(model_config) as cfg:
        training_config = json.load(cfg)
    return training_config


def predict_single_image(net, image):
    prediction = net(image)

    prediction = prediction.view(4, -1)
    prediction = prediction.detach().cpu().numpy()

    return prediction


def predict_on_index(net, image, label):
    prediction = predict_single_image(net, image)

    x_pred = prediction[:, 0]
    y_pred = prediction[:, 1]

    label = label.view(4, -1).cpu().numpy()

    x_true = label[:, 0]
    y_true = label[:, 1]

    return x_true, y_true, x_pred, y_pred


def predict_on_batch(net, images):
    prediction = net(images)

    prediction = prediction.view(-1, 4, 2)
    prediction = prediction.detach().cpu().numpy()

    x_pred = prediction[:, :, 0]
    y_pred = prediction[:, :, 1]

    return x_pred, y_pred