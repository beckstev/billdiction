import torch.nn as nn
import torch.nn.functional as F

from torchvision.models import vgg11, vgg19_bn, resnet18, resnet50, resnet101


def num_flat_features(x):
    """
        Returns the number of outputs of a layer. Purpose is flattening layers after convolution.

    :param x: (ndarray) output vector
    :return: (int) size of the vector
    """
    size = x.size()[1:]  # all dimensions except the batch dimension
    num_features = 1
    for s in size:
        num_features *= s
    return num_features


def init_weights(model, method='kaiming'):
    """
        Initializes weights according to sophisticated methods such as kaiming or xavier initialization.
        See for example https://www.cv-foundation.org/openaccess/content_iccv_2015/html/He_Delving_Deep_into_ICCV_2015_paper.html

    :param model: a pytorch model with iteration functionality
    :param method: initialization method
    :return: 0
    """

    for layer in model:
        if type(layer) == nn.Linear or type(layer) == nn.Conv2d:
            if method == 'kaiming':
                nn.init.kaiming_normal_(layer.weight)
            elif method == 'xavier':
                nn.init.xavier_normal_(layer.weight)


class IdentityModule(nn.Module):
    def forward(self, x):
        return x


class MiniNN(nn.Module):
    def __init__(self):
        super(MiniNN, self).__init__()

        C_in, C1, C2 = 1, 24, 48

        self.conv_layers = nn.Sequential(
            nn.Conv2d(in_channels=C_in, out_channels=C1, kernel_size=7, stride=2, padding=3),
            nn.BatchNorm2d(C1),
            nn.ReLU(),
            nn.MaxPool2d(kernel_size=2, stride=2),

            nn.Conv2d(in_channels=C1, out_channels=C2, kernel_size=5, padding=1),
            nn.BatchNorm2d(C2),
            nn.ReLU(),
            nn.MaxPool2d(kernel_size=2, stride=2)
        )
        H_in, H1,  D_out = 82128, 512, 8

        self.fc_layers = nn.Sequential(
            nn.Linear(H_in, H1),
            nn.BatchNorm1d(H1),
            nn.ReLU(),
            nn.Linear(H1, D_out),
        )

        init_weights(self.conv_layers, method='kaiming')
        init_weights(self.fc_layers, method='kaiming')

    def forward(self, x):
        x = self.conv_layers(x)
        x = x.view(-1, num_flat_features(x))
        x = self.fc_layers(x)
        return x


class MiniNNv2(nn.Module):
    def __init__(self):
        super(MiniNNv2, self).__init__()

        # convolutional layers
        C_in, C1, C2, C3 = 1, 4, 8, 16
        self.conv_layers = nn.Sequential(
            nn.Conv2d(in_channels=C_in, out_channels=C1, kernel_size=7, stride=2, padding=3),
            nn.BatchNorm2d(C1),
            nn.ReLU(),
            nn.Conv2d(in_channels=C1, out_channels=C1, kernel_size=5, padding=2),
            nn.BatchNorm2d(C1),
            nn.ReLU(),
            nn.MaxPool2d(kernel_size=2, stride=2),

            nn.Conv2d(in_channels=C1, out_channels=C2, kernel_size=3, padding=1),
            nn.BatchNorm2d(C2),
            nn.ReLU(),
            nn.Conv2d(in_channels=C2, out_channels=C2, kernel_size=3, padding=1),
            nn.BatchNorm2d(C2),
            nn.ReLU(),
            nn.MaxPool2d(kernel_size=2, stride=2),

            nn.Conv2d(in_channels=C2, out_channels=C3, kernel_size=3, padding=1),
            nn.BatchNorm2d(C3),
            nn.ReLU(),
            nn.Conv2d(in_channels=C3, out_channels=C3, kernel_size=3, padding=1),
            nn.BatchNorm2d(C3),
            nn.ReLU(),
            nn.MaxPool2d(kernel_size=2, stride=2)
        )
        init_weights(self.conv_layers, method='kaiming')

        # fully connected layers
        H_in, H1, H2, D_out = C3 * (240//16) * (480//16), 1024, 256, 8
        self.fc_layers = nn.Sequential(
            nn.Dropout(),
            nn.Linear(H_in, H1),
            nn.BatchNorm1d(H1),
            nn.ReLU(),

            nn.Dropout(),
            nn.Linear(H1, D_out),
        )
        init_weights(self.fc_layers, method='kaiming')

    def forward(self, x):
        x = self.conv_layers(x)
        x = x.view(-1, num_flat_features(x))
        x = self.fc_layers(x)
        return x


class MiniNNv3(nn.Module):
    def __init__(self):
        super(MiniNNv3, self).__init__()

        # convolutional layers
        C_in, C1, C2, C3 = 1, 8 , 16, 32
        self.conv_layers = nn.Sequential(
            nn.Conv2d(in_channels=C_in, out_channels=C1, kernel_size=7, stride=2, padding=3),
            nn.BatchNorm2d(C1),
            nn.ReLU(),
            nn.Conv2d(in_channels=C1, out_channels=C1, kernel_size=5, padding=2),
            nn.BatchNorm2d(C1),
            nn.ReLU(),
            nn.Conv2d(in_channels=C1, out_channels=C1, kernel_size=1),
            nn.BatchNorm2d(C1),
            nn.ReLU(),
            nn.MaxPool2d(kernel_size=2, stride=2),

            nn.Conv2d(in_channels=C1, out_channels=C2, kernel_size=3, padding=1),
            nn.BatchNorm2d(C2),
            nn.ReLU(),
            nn.Conv2d(in_channels=C2, out_channels=C2, kernel_size=3, padding=1),
            nn.BatchNorm2d(C2),
            nn.ReLU(),
            nn.Conv2d(in_channels=C2, out_channels=C2, kernel_size=1),
            nn.BatchNorm2d(C2),
            nn.ReLU(),
            nn.MaxPool2d(kernel_size=2, stride=2),

            nn.Conv2d(in_channels=C2, out_channels=C3, kernel_size=3, padding=1),
            nn.BatchNorm2d(C3),
            nn.ReLU(),
            nn.Conv2d(in_channels=C3, out_channels=C3, kernel_size=3, padding=1),
            nn.BatchNorm2d(C3),
            nn.ReLU(),
            nn.Conv2d(in_channels=C3, out_channels=C3, kernel_size=1),
            nn.BatchNorm2d(C3),
            nn.ReLU(),
            nn.MaxPool2d(kernel_size=2, stride=2)
        )
        init_weights(self.conv_layers, method='kaiming')

        # fully connected layers
        H_in, H1, H2, D_out = C3 * (240//16) * (480//16), 1024, 256, 8
        self.fc_layers = nn.Sequential(
            nn.Dropout(p=0.5),
            nn.Linear(H_in, H1),
            nn.BatchNorm1d(H1),
            nn.ReLU(),

            nn.Dropout(p=0.3),
            nn.Linear(H1, H2),
            nn.BatchNorm1d(H2),
            nn.ReLU(),

            nn.Dropout(p=0.2),
            nn.Linear(H2, D_out),
        )
        init_weights(self.fc_layers, method='kaiming')

    def forward(self, x):
        x = self.conv_layers(x)
        x = x.view(-1, num_flat_features(x))
        x = self.fc_layers(x)
        return x


class ResNet18(nn.Module):
    def __init__(self, use_pretrained_weights=True):
        super(ResNet18, self).__init__()

        # layer dimensions
        H_in, H1, H2, H_out = 9216, 1024, 256, 8

        self.model = resnet18(pretrained=use_pretrained_weights)
        self.model.avgpool = nn.AvgPool2d(7, stride=1)
        self.model.fc = nn.Linear(H_in, H1)

        self.fc2 = nn.Sequential(
            nn.Dropout(0.3),
            nn.Linear(H1, H2),
            nn.BatchNorm1d(H2),
            nn.ReLU(),

            nn.Dropout(0.3),
            nn.Linear(H2, H_out))

    def forward(self, x):
        x = self.model(x)
        x = self.fc2(x)
        return x

class ResNet50(nn.Module):
    def __init__(self, use_pretrained_weights=True):
        super(ResNet50, self).__init__()

        # layer dimensions
        H_in, H1, H2, H_out = 36864, 4096, 512, 8

        self.model = resnet50(pretrained=use_pretrained_weights)
        self.model.fc = nn.Linear(H_in, H1)
        self.fc2 = nn.Sequential(
            nn.BatchNorm1d(H1),
            nn.ReLU(),
            nn.Linear(H1, H2),
            nn.BatchNorm1d(H2),
            nn.ReLU(),
            nn.Linear(H2, H_out))

    def forward(self, x):
        x = self.model(x)
        x = self.fc2(x)
        return x

class ResNet101(nn.Module):
    def __init__(self, use_pretrained_weights=True):
        super(ResNet101, self).__init__()
        # layer dimensions
        H_in, H1, H2, H_out = 36864, 1024, 512, 8
        self.model = resnet101(pretrained=use_pretrained_weights)
        self.model.fc = nn.Linear(H_in, H1)
        self.fc2 = nn.Sequential(
            nn.BatchNorm1d(H1),
            nn.ReLU(),
            nn.Linear(H1, H2),
            nn.BatchNorm1d(H2),
            nn.ReLU(),
            nn.Linear(H2, H_out))

    def forward(self, x):
        x = self.model(x)
        x = self.fc2(x)
        return x

class FasterRCNN(nn.Module):
    def __init__(self, use_pretrained_weights=True):
        super(FasterRCNN, self).__init__()

        # network parameters
        self.n = 3
        self.anchors = 1

        self.resnet = resnet18(pretrained=use_pretrained_weights)
        self.backbone = nn.Sequential(
            self.resnet.conv1,
            self.resnet.bn1,
            self.resnet.relu,
            self.resnet.maxpool,
            self.resnet.layer1,
            self.resnet.layer2,
            self.resnet.layer3,
            self.resnet.layer4,
            self.resnet.avgpool
        )

        self.sliding_window = nn.Sequential(
            nn.Conv2d(512, 256, kernel_size=self.n, padding=1),
            nn.ReLU()
        )

        self.reg = nn.Sequential(
            nn.Conv2d(256, 4 * self.anchors, kernel_size=1)
        )
        self.clf = nn.Sequential(
            nn.Conv2d(256, 2 * self.anchors, kernel_size=1),
            nn.LogSoftmax(dim=1)
        )

    def forward(self, x):
        x = self.backbone(x)
        x = self.sliding_window(x)
        clf = self.clf(x)
        reg = self.reg(x)
        return clf, reg
