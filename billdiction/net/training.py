import numpy as np
import pandas as pd
import os
import json

from datetime import datetime
from pathlib import Path

import torch
from torch.utils.data import DataLoader
from torch.optim import SGD, RMSprop
from torch.optim.lr_scheduler import ReduceLROnPlateau

# BillDiction Library
from billdiction.net import networks
from billdiction.io.dataset import get_dataset
from billdiction.evaluation.plotting import plot_learning_curve
from billdiction.net.losses import MeanSquaredDistanceLoss, SmoothMeanDistanceLoss, FasterRCNNLoss


def memory_monitor():
    print("Allocated Memory:", torch.cuda.memory_allocated() / 1024 / 1024)
    print("Cached Memory:", torch.cuda.memory_cached() / 1024 / 1024, "\n")


def loss_log(loss, mean_list, std_list):
    loss = np.array(loss)
    mean_list.append(loss.mean())
    std_list.append(loss.std())


def get_optimizer(net, learning_rate, weight_decay, batch_size, device, training_type):
    if training_type == 'corners':
        criterion = SmoothMeanDistanceLoss()
        optimizer = RMSprop(net.parameters(), lr=learning_rate, weight_decay=weight_decay)

    elif training_type == 'sum':
        criterion = FasterRCNNLoss(N_reg=15*7, N_cls=batch_size, device=device)
        optimizer = SGD(net.parameters(), lr=learning_rate, momentum=0.9, weight_decay=weight_decay)

    else:
        raise NotImplementedError

    return criterion, optimizer


def train(training_parameters, **kwargs):
    architecture = training_parameters['architecture']
    training_type = training_parameters['training_type']
    batch_size = training_parameters['batch_size']
    learning_rate = training_parameters['learning_rate']
    weight_decay = training_parameters['weight_decay']
    n_epochs = training_parameters['n_epochs']
    transforms_train = training_parameters['transforms_train']
    transforms_val = training_parameters['transforms_val']
    use_rgb = training_parameters['use_rgb']
    lr_schedule = training_parameters['learning_rate_schedule']

    early_stopping = {'patience': training_parameters['early_stopping_patience'],
                      'delta': training_parameters['early_stopping_delta'],
                      'best_epoch': 0,
                      'best_loss': -1}

    # set a timestamp for saving the model later on
    training_timestamp = datetime.now().strftime('%Y-%m-%d_%H:%M:%S')

    # define directory to save training outcome
    save_base_path = os.path.join(Path(os.path.abspath(__file__)).parents[2],
                                  "saved_models", architecture, training_timestamp)
    if not os.path.isdir(save_base_path):
        os.makedirs(save_base_path)

    # save training parameters
    del training_parameters['transforms_train'], training_parameters['transforms_val']
    with open(os.path.join(save_base_path, "training_config.json"), "w") as f:
        json.dump(training_parameters, f)

    save_model_path = os.path.join(save_base_path, "{}.state".format(architecture))

    # get training data
    path_to_data = os.path.join(Path(os.path.abspath(__file__)).parents[2], "bills")
    train_data = get_dataset(path_to_data, usage='train', transforms=transforms_train, use_rgb=use_rgb,
                             training_type=training_type)
    val_data = get_dataset(path_to_data, usage='val', transforms=transforms_val, use_rgb=use_rgb,
                           training_type=training_type)

    train_loader = DataLoader(train_data,
                              batch_size=batch_size,
                              shuffle=True,
                              num_workers=5,
                              drop_last=True)
    val_loader = DataLoader(val_data,
                            batch_size=batch_size,
                            shuffle=False,
                            num_workers=5,
                            drop_last=True)

    # Compute on GPU if available
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

    # initialize deep learning model
    net = getattr(networks, architecture)()
    net.train()
    net.to(device)
    print(f'Training on: {device}')
    print('Model Memory Allocation')
    if torch.cuda.is_available():
        memory_monitor()

    # track learning curve
    train_loss_mean = []
    train_loss_std = []
    val_loss_mean = []
    val_loss_std = []

    # define training environment

    criterion, optimizer = get_optimizer(net=net,
                                         learning_rate=learning_rate,
                                         weight_decay=weight_decay,
                                         batch_size=batch_size,
                                         device=device,
                                         training_type=training_type)

    scheduler = ReduceLROnPlateau(optimizer,
                                  verbose=True,
                                  factor=lr_schedule['factor'],
                                  patience=lr_schedule['patience'])

    # train the model
    print(f"Begin training on {len(train_data)} samples")
    for i, epoch in enumerate(range(n_epochs)):
        net.train()
        train_loss = []
        for batch in train_loader:
            X, y = batch
            X, y = X.to(device), y.to(device)

            optimizer.zero_grad()
            output = net(X)
            loss = criterion(output, y)
            loss.backward()
            optimizer.step()

            train_loss.append(loss.item())

        # store training loss
        loss_log(train_loss, train_loss_mean, train_loss_std)

        # validate after each epoch
        net.eval()
        val_loss = []
        for batch in val_loader:
            X, y = batch
            X, y = X.to(device), y.to(device)

            output = net(X)
            loss = criterion(output, y)

            val_loss.append(loss.item())

        # store validation loss
        loss_log(val_loss, val_loss_mean, val_loss_std)

        # monitor loss evolution
        print('Training Epoch %i, Training Loss: %.5f +- %.5f, Validation Loss: %.5f +- %.5f' %
              (epoch + 1,
               train_loss_mean[i], train_loss_std[i],
               val_loss_mean[i], val_loss_std[i]))
        # learning rate scheduler
        scheduler.step(val_loss_mean[i])
        # early stopping
        if i == 0:
            early_stopping['best_loss'] = val_loss_mean[i]
            early_stopping['best_epoch'] = i

        if val_loss_mean[i] < early_stopping['best_loss']:
            early_stopping['best_loss'] = val_loss_mean[i]
            early_stopping['best_epoch'] = i
            torch.save(net.state_dict(), save_model_path)

        if i >= early_stopping['best_epoch'] + early_stopping['patience']:
            print('Stopping early. Saved model of epoch {}'.format(early_stopping['best_epoch'] + 1))
            break


    print('Finished Training')
    print(f'Saving model to {save_base_path}')

    # save monitoring
    save_log_path = os.path.join(save_base_path, 'monitor.csv')
    monitor_df = pd.DataFrame(data={"train_loss_mean": train_loss_mean,
                                    "train_loss_std": train_loss_std,
                                    "val_loss_mean": val_loss_mean,
                                    "val_loss_std": val_loss_std})
    monitor_df.index.name = 'epoch'
    monitor_df.index = monitor_df.index + 1
    monitor_df.to_csv(save_log_path)

    # plot learning curve
    save_learning_curve_path = os.path.join(save_base_path, "learning_curve.png")
    plot_learning_curve(monitor_df, save_learning_curve_path, ylog=False)
