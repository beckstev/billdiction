import torch
import torchvision.transforms.functional as F

import billdiction.net.utils as utils


class CornerNN:
    def __init__(self, model_path):
        self.model_path = model_path

        # get details from the config
        self.model_config = utils.load_config(self.model_path)
        self.original_size = self.model_config['original_image_size']

        # define the actual model
        self.device = utils.get_device()
        self.model = self._load_model()

    def __call__(self, x):
        return self.infer(x)

    def infer(self, images):
        assert len(images.shape) == 3 or len(images.shape) == 4, f"Tensor shape {images.shape} has to be of length 3 or 4"

        if len(images.shape) == 3:
            x = self.infer_single(images)
        else:
            x = self.infer_batch(images)

        return x

    def infer_single(self, image):
        x = torch.stack([image, self.get_horizontal_flip(image)])

        if x.device != self.device:
            x = x.to(self.device, dtype=torch.float32)

        x = self.model(x)

        x[1, 0::2] = self.original_size[0] - x[1, [2, 0, 6, 4]]
        x[1, 1::2] = x[1, [3, 1, 7, 5]]

        x = x.mean(dim=0)
        return x

    def infer_batch(self, images):
        x = self.get_horizontal_flip_batch(images)

        if x.device != self.device:
            x = x.to(self.device, dtype=torch.float32)

        x = self.model(x)
        x = self.get_flipped_batch_means(x)

        return x

    def get_flipped_batch_means(self, labels):
        """
        Averages labels for the same image for horizontally augmented data:
        [label1, label1_flip, label2, label2_flip, ...] becomes [mean(label1, label1_flip), mean(label2, label2_flip), ...]

        :param labels: torch.tensor of shape (2N, 8)
        :return: torch.tensor of shape (N, 8)
        """

        # flip labels
        l = labels[0::2]
        l_flipped = labels[1::2]
        l_flipped[:, 0::2] = self.original_size[0] - l_flipped[:, [2, 0, 6, 4]]
        l_flipped[:, 1::2] = l_flipped[:, [3, 1, 7, 5]]

        means = (l + l_flipped) / 2

        return means

    def get_horizontal_flip_batch(self, images):
        """
        Adds horizontally flipped images to the tensor:
        [img1, img2, ...] becomes [img1, img1_flip, img2, img2_flip]

        :param images: torch.tensor of shape (N, C, H, W)
        :return: torch.tensor of shape (2N, C, H, W)
        """

        shape = (images.shape[0] * 2, *images.shape[1:])
        batch = torch.zeros(size=shape)

        for i in range(len(images)):
            batch[2 * i] = images[i]
            batch[2 * i + 1] = self.get_horizontal_flip(images[i])

        return batch

    def get_horizontal_flip(self, image):
        """
        Horizontally flips a tensor

        :param image: torch.tensor of shape (C, H, W)
        :return: torch.tensor of shape (C, H, W)
        """

        return torch.flip(image, dims=[0, 2])

    def _load_model(self):
        # TODO: refactor visualization pipe and this one here, so that no code is duplicated

        # collect relevant variables from parameters
        architecture = self.model_config['architecture']

        # load model
        net = utils.load_model(model_path=self.model_path,
                               architecture=architecture,
                               evaluation=True)

        return net
