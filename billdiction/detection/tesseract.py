import pytesseract
import time
from multiprocessing import Pool, cpu_count

from billdiction.detection.utils import crop_image, rescale_image


def read_region(img_rescaled, bbox, padding):
    # crop image to region of interest
    roi = crop_image(img_rescaled, bbox, padding=padding)

    # in order to apply Tesseract v4 to OCR text we must supply
    # (1) a language, (2) an OEM flag of 4, indicating that the we
    # wish to use the LSTM neural net model for OCR, and finally
    # (3) an OEM value, in this case, 7 which implies that we are
    # treating the ROI as a single line of text
    tesseract_config = ("-l deu -h 7")

    # extract text
    roi_text = pytesseract.image_to_string(roi, config=tesseract_config)

    return roi_text, roi


def tesseract_character_readout(image, bboxes, rescale_width=0, threads=2):
    """
    Using tesseract to readout character of a given bounding boxes

    :param image:
    :param bboxes: text region bounding boxes (N, 4) with boxes (x, y, w, h)
    :param rescale_width: (default: no rescaling) width for the image to be rescaled
    :param threads: how many threads to use for tesseract inference
    :return: array of string
    """
    width_original, _ = image.shape[:2]
    img_rescaled = rescale_image(image, rescale_width=rescale_width)

    # fit bboxes for the rescaled image
    scaling = rescale_width / width_original if rescale_width > 0 else 1
    bboxes_rescaled = bboxes * scaling
    padding = int(img_rescaled.shape[1] / 200)

    text, text_regions = [], []
    start = time.time()

    threads = threads if threads else cpu_count()
    with Pool(processes=threads) as pool:
        results = pool.starmap(read_region, [[img_rescaled, bbox, padding] for bbox in bboxes_rescaled])

    print(f"tesseract inference on {threads} threads took {round(time.time() - start, 2)} seconds")

    for t, r in results:
        text.append(t)
        text_regions.append(r)

    return text, text_regions