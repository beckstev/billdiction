import numpy as np
import cv2 as cv

# BillDiction Library
from billdiction.detection.utils import rescale_image, HierarchyTree


def find_contours(image, rescale_ratio, aspect_ratio, width, pad_x, pad_y, original_shape):
    # find contours in the thresholded image and sort them by their size
    cnts, hierarchy = cv.findContours(image, cv.RETR_CCOMP, cv.CHAIN_APPROX_SIMPLE)
    cnts = sorted(cnts, key=cv.contourArea, reverse=True)

    # utilities
    bboxes = []

    # TODO: Utilize the HTree to reduce character proposals, e.g. drop hole in characters such as A, B, O
    if len(cnts) > 0:
        htree = HierarchyTree(hierarchy)
        print(f"Hierarchy Tree depth: {htree.depth()}")
        print(f"Hierarchy Tree length: {len(htree)} (contours: {len(cnts)})")
        for i, level in enumerate(htree.levels):
            idx = level['idx']
            print(f"Level {i}: {len(idx)}")

    # loop over the contours
    for i, c in enumerate(cnts):
        # compute the bounding box of the contour and use the contour to
        # compute the aspect ratio and coverage ratio of the bounding box
        # width to the width of the image
        (x, y, w, h) = cv.boundingRect(c)
        ar = w / float(h)
        crWidth = w / float(image.shape[1])

        # check to see if the aspect ratio and coverage width are within
        # acceptable criteria
        if ar > aspect_ratio and crWidth > width and c.shape[1] != 0:

            # pad the bounding box since we applied erosions and now need
            # to re-grow it
            pX = int((x + w) * pad_x)
            pY = int((y + h) * pad_y)
            (x, y) = (x - pX, y - pY)

            # Bounding boxes outside the image are non-sense and tesseract cant
            # handle them
            if x < 0:
                x = 0
            if y < 0:
                y = 0
            (w, h) = (w + (pX * 2), h + (pY * 2))

            # extract the ROI from the image and draw a bounding box
            # surrounding the MRZ
            # Because we wanna use the regions of the raw image we have to
            # transform height, width, ...
            box = np.array((x, y, w, h)) * rescale_ratio
            bboxes.append(box)

    if bboxes:
        bboxes = np.stack(bboxes)
        # drop the bbox of the whole bill
        height, width = original_shape
        areas = bboxes[:, 2] * bboxes[:, 3]
        bboxes = bboxes[areas < 0.7 * height * width]

    else:
        bboxes = np.zeros(shape=(0, 4))

    return bboxes


def get_association_box(region, characters, mask):
    """
    Given a list of text regions and a mask, it returns the smallest bounding box that
    contains all regions according to the mask

    :param region: bounding box of the regions that characters shall be associated with
    :param characters: (N, 4) array of text regions, (x, y, w, h)
    :param mask: (N, ) array selecting the regions to draw a box around
    :return: tightest bounding box for all relevant regions
    """
    associated = characters[mask]

    region_xmin = min(associated[:, 0].min(), region[0])
    region_ymin = min(associated[:, 1].min(), region[1])
    region_xmax = max(associated[:, 2].max(), region[0] + region[2])
    region_ymax = max(associated[:, 3].max(), region[1] + region[3])

    box = np.array((region_xmin, region_ymin,
                    region_xmax - region_xmin, region_ymax - region_ymin))

    return box


def associate_characters_by_left_right(bboxes, search_distance):
    """
    Associate characters to regions of text

    :param bboxes: np.array of shape (N, 4) with bounding boxes (x, y, w, h)
    :param search_distance: (x, y)-distance in pixel to look for characters
    :return: np.array of shape (N, 4) storing regions of text
    """
    x_dist, y_dist = search_distance

    # characters are handled as (xmin, ymin, xmax, ymax)
    characters = np.zeros_like(bboxes)
    characters[:, :2] = bboxes[:, :2]
    characters[:, 2:] = bboxes[:, :2] + bboxes[:, 2:]

    associations = []
    while len(characters) > 0:
        # while bounding boxes are handled as (xmin, ymin, width, height)
        (xmin, ymin, xmax, ymax) = characters[0]
        region = np.array([xmin, ymin, xmax - xmin, ymax - ymin])

        # prevent infinite while loops for unassociable characters
        first_iter = True

        while True:
            xmin, ymin = region[:2]
            xmax, ymax = region[:2] + region[2:]

            xmask = np.logical_or(characters[:, 2] <= xmin - x_dist,
                                  characters[:, 0] >= xmax + x_dist)

            ymask = np.logical_or(characters[:, 3] <= ymin - y_dist,
                                  characters[:, 1] >= ymax + y_dist)

            mask = np.logical_and(~xmask, ~ymask)

            # make sure that there are any characters associated
            if mask.any():
                # get box around associated characters
                region = get_association_box(region, characters, mask)

                # remove associated characters from the array
                characters = characters[~mask]
                first_iter = False

            else:
                # add region to associations and end the iteration
                if first_iter:
                    characters = characters[1:]
                associations.append(region.copy())
                break

    associations = np.stack(associations) if associations else np.zeros(shape=(0, 4))

    return associations


def segment_characters(image, rescale_width, xdist=30, ydist=0):
    """
    This algorithm is based on the prior knowledge, that all characters are of same size and monospaced

    :param image: top-down-view image to be segmented
    :param rescale_width: width to which the image is rescaled prior to segmentation
    :param xdist: [0, 100] distance for associating neighbors (in percentage of image width).
                  100 will associate whole line, while 0 will only associate a character itself
    :param ydist: [0, 100] distance for associating neighbors (in percentage of image width).
                  100 will associate whole columns, while 0 will only associate a character itself
    :return: processed image and bounding boxes
    """

    # rescale image for segmentation
    ratio = image.shape[1] / rescale_width
    rescaled_img = rescale_image(image, rescale_width=rescale_width)

    # preprocessing
    blur = cv.GaussianBlur(rescaled_img, (3, 3), 0)
    inv_blur = cv.bitwise_not(blur)
    canny = cv.Canny(inv_blur, 100, 200)

    characters = find_contours(canny,
                               rescale_ratio=ratio,
                               aspect_ratio=0,
                               width=0,
                               pad_x=0,
                               pad_y=0,
                               original_shape=image.shape)

    # drop contours that are at the boundary of the image
    characters = drop_contours_at_boundary(characters, image.shape, eps=image.shape[1] / 100)

    # associate characters to rows
    height, width = image.shape
    x_dist, y_dist = width / 100 * xdist, height / 100 * ydist

    regions = associate_characters_by_left_right(characters, search_distance=(x_dist, y_dist))
    regions = reject_regions(regions, min_threshold=0.5, max_threshold=10)

    return canny, regions, characters


def drop_contours_at_boundary(contours, image_size, eps=10):
    """
    It appears that there are contours detected at the edges of a bill. We want to reject them

    :param contours: (N, 4) array of bounding boxes (x, y, w, h)
    :param image_size: (height, width)
    :param eps: epsilon (number of pixels) to control rejection sensitivity
    :return: reduced contours
    """

    # drop contours that are at the boundary of an image
    left_mask = contours[:, 0] > eps
    right_mask = image_size[1] - (contours[:, 0] + contours[:, 2]) > eps
    top_mask = contours[:, 1] > eps
    bottom_mask = image_size[0] - (contours[:, 1] + contours[:, 3]) > eps

    boundary_mask = left_mask & right_mask & top_mask & bottom_mask

    # drop contours that are caused by bill boundary
    aspect_ratios = contours[:, 3] / contours[:, 2]
    aspect_mask = np.logical_and(aspect_ratios < 5, aspect_ratios > 1/5)

    hw_max = np.maximum(contours[:, 2], contours[:, 3])
    hw_mask = hw_max < image_size[1] * 0.2

    mask = boundary_mask & (hw_mask | aspect_mask)

    return contours[mask]


def reject_regions(regions, min_threshold, max_threshold):
    """
    Rejects regions that are not part of the current detection task

    :param regions: (N, 4) array with bounding boxes of text regions
    :param min_threshold: min threshold (0, inf) for rejection (min_threshold < max_threshold)
    :param max_threshold: max threshold (0, inf) for rejection (min_threshold < max_threshold)
    :return: selected regions
    """
    heights = regions[:, 3]
    h_med = np.median(heights)
    if heights.size:
        h_mask = np.logical_and(heights > min_threshold * h_med, heights < max_threshold * h_med)
    else:
        h_mask = []

    widths = regions[:, 2]
    w_med = np.median(widths)
    if widths.size:
        w_mask = np.logical_and(widths > min_threshold * w_med, widths < max_threshold * w_med)
    else:
        w_mask = []

    mask = np.logical_and(h_mask, w_mask)

    selected = regions[mask]

    return selected


def segment_canny_closed(image, rescale_width):
    """
    Alternative segmentation algorithm based on the Canny edge detector

    :param image: top-down-view image to be segmented
    :param rescale_width: width to which the image is rescaled prior to segmentation
    :return: processed image and bounding boxes
    """
    # initialize kernels
    rectKernel = cv.getStructuringElement(cv.MORPH_RECT, (9, 3))

    # rescale image for segmentation
    ratio = image.shape[1] / rescale_width
    rescaled_img = rescale_image(image, rescale_width=rescale_width)

    blur = cv.GaussianBlur(rescaled_img, (3, 3), 0)
    canny = cv.Canny(blur, 100, 200)
    closed = cv.morphologyEx(canny, cv.MORPH_CLOSE, rectKernel)

    bboxes = find_contours(image=closed,
                           rescale_ratio=ratio,
                           aspect_ratio=0.3,
                           width=0.05,
                           pad_x=0.02,
                           pad_y=0.01)

    return closed, bboxes


def is_centered(bboxes, width, epsilon=0.1):
    """
    Returns a mask that decides whether a bounding box is centered in x direction or not

    :param bboxes: (np.ndarray of shape (N, 4) list of bounding boxes (x, y, w, h)
    :param width: width of the original image
    :param epsilon: (0 < float < 1)
    :return: mask, is true if a bounding box is centered
    """

    centers = bboxes[:, 0] + bboxes[:, 2] / 2
    mask = np.abs(centers - width / 2) < epsilon * width

    return mask


def segment_barcode(bboxes, width):
    """
    Detects a barcode based on bbox detections of an edge detector such as Canny
    Will fail if no barcode is present, and should thus be supplemented by an image level detector

    :param bboxes: (np.ndarray of shape (N, 4) list of characters (e.g. detected by the Canny algorithm)
    :param width: width of the original image
    :return: bounding box for barcode (x, y, w, h)
    """

    # hyperparameters of the function
    aspect_ratio, height_eps, y_eps = 5, 0.5, 0.05

    try:
        sort = np.argsort(bboxes[:, 3])
        stripes = bboxes[sort][::-1]

        # keep only bboxes with a certain aspect ratio
        aspect_mask = stripes[:, 3] / stripes[:, 2] > aspect_ratio
        stripes = stripes[aspect_mask]

        # keep only bboxes with height different from the median
        h = np.median(stripes[:, 3])
        height_mask = np.abs(stripes[:, 3] - h) < height_eps * h
        stripes = stripes[height_mask]

        # keep only bboxes that are at a similar y position
        ypos = np.median(stripes[:, 1])
        y_mask = np.abs(stripes[:, 1] - ypos) < y_eps * width
        stripes = stripes[y_mask]

        xmin, ymin = stripes[:, 0].min(), stripes[:, 1].min()
        xmax, ymax = (stripes[:, 0] + stripes[:, 2]).max(), (stripes[:, 1] + stripes[:, 3]).max()

        # reject misclassifications
        if (xmin + xmax) / 2 - width < width / 10 and len(stripes) > 10:
            barcode = np.array([xmin, ymin, xmax - xmin, ymax - ymin])
        else:
            barcode = np.zeros((0, 4), dtype=np.float32)

        return barcode

    except ValueError:
        return np.zeros((0, 4), dtype=np.float32)


def area_ysegmentation(df_bboxes, sigma=1):
    ''' Function to detect paragraph edges. The mean y distance
        between two sequentially bounding boxes is used.
    :param df_bboxes: Dataframe which contains the bounding boxes
    :param sigma: range in which sections are rejected
    :return section_edge: Numpy array containg the edge position
    '''
    height = df_bboxes['h'].values
    y = df_bboxes['y']

    assert y.is_monotonic, "df_bboxes has to be sorted by y values"
    y = y.values

    y_lower = y + height

    # We wanna detect the paragraph edges by using the mean distance
    # between two sequentially bboxes
    delta_y = y[1:] - y_lower[:-1]

    # Ignore bboxes on the same height, because this causes a negative delta y
    mean = np.mean(delta_y[delta_y > 0])
    std = np.std(delta_y[delta_y > 0])

    section_edge = y_lower[:-1] + 1/2 * delta_y
    section_edge = section_edge[delta_y >= mean + sigma * std]
    section_edge = np.concatenate([[y[0]], section_edge, [y_lower[-1]]])

    return section_edge


def get_nn_boxes(bbox, bboxes, xeps, yeps):
    ''' Function to find neighboring bbox of the same height/line.
        :param bbox: Numpy array containing the coordinates of the bbox
                     where we wanna find neighbors
        :param bboxes: Numpy array containing the coordinates of all bboxes
                       we wanna check
        :param xeps: Defines the maximum xdelta to bboxes must have two be
                     considered as neighbors
        :param yeps: Defines the maximum ydelta to bboxes can have two be
                     considered as neighbors
        :return mask: Returning a mask which only contains a True value
                      for neighbors
    '''
    y = bboxes[:, 1]
    x = bboxes[:, 0]
    ybox = bbox[1]
    xbox = bbox[0]

    # First we wanna search for neighbors in the y direction
    # We have to exclude the bbox, because it is his own neighbor
    mask_y = np.abs(y[y != ybox] - ybox) <= yeps

    # Second, we wanna search for neighbors in the x direction
    # We use ">=" to exclude bbox on the same x level. This helps to improve
    # the stability of the search because sometimes, caused by wrong/bad
    # segmentation, two bounding will be considerd to be in the same line but
    # actually their are not.
    mask_x = np.abs(x[y != ybox] - xbox) >= xeps

    mask = mask_x & mask_y
    return mask


def get_mean_xdelta(df_bboxes, xeps, yeps):
    '''Function to get the mean x distance between to sequential bbox.
       :param df_bboxes: Dataframe which contains the cooridnates of the bboxes
                         we wanna us
       :param xeps: Defines the maximum xdelta to bboxes must have two be
                    considered as neighbors
       :param yeps: Defines the maximum ydelta to bboxes can have two be
                    considered as neighbors
       :return mean_delta_x: Returns the x distance mean of the df_bboxes
    '''
    x = df_bboxes['x'].values
    y = df_bboxes['y'].values
    bboxes = df_bboxes.loc[:, 'x':'h'].values
    delta_x = []

    # We are looping over all bboxes to consider only the distance of bboxes
    # which are on the same height level. Therefore, the function get_nn_boxes
    # is used.
    # TODO: remove the loop later on as we will write number of row in the dataframe
    for i, bbox in enumerate(bboxes):
        xbox = bbox[0]
        ybox = bbox[1]
        mask = get_nn_boxes(bbox, bboxes, xeps, yeps)
        x_boxes = x[y != ybox]
        x_nnboxes = x_boxes[mask]
        if len(x_nnboxes) is not 0:
            delta_x_around = np.abs(xbox - x_nnboxes).tolist()
            delta_x += delta_x_around

    if len(delta_x) > 0:
        mean_delta_x = np.mean(delta_x)
    else:
        mean_delta_x = np.nan

    return mean_delta_x


def segment_section_columns(df_bboxes_in, local_mean_delta_x, global_mean_delta_x):
    ''' Function to predict the label of a bbox by using it x-position. Therefore,
        the section has to be vertically separable.
        :param df_bboxes_in: Bboxes inside the section
        :param local_mean_delta_x: Mean x distance between bboxes in the section
                                   of the df_bboxes_in
        :param global_mean_delta_x: Mean x distance of the bill
        :return df_bboxes_in: Returns the df_bboxes_in with modified labels if
                              the local x distance is greater as the global one.
    '''
    dtype = np.uint8
    if local_mean_delta_x > global_mean_delta_x:
        x = df_bboxes_in['x'].values
        mask = x > local_mean_delta_x

        return mask.astype(dtype)

    else:
        return np.zeros(len(df_bboxes_in), dtype=dtype)


def area_segmentation(df_bboxes, height_image_warped, xeps=50, yeps=35):
    ''' Function to divide the bill into different section and to use this
        information to predict the label of some bboxes.
        :param df_bboxes: DataFrame which contains the bboxes
        :param height_image_warped: Height of the warped image.
                                    This param is needed to normalize the
                                    the edges of a section. The normalization is
                                    required due to axvline y-limits.
         :param xeps: Defines the maximum xdelta to bboxes must have two be
                      considered as neighbors
         :param yeps: Defines the maximum ydelta to bboxes can have two be
                      considered as neighbors
         :return segmentation: Dict which contains the hcut and vcut.
         :return df_bboxes: Returns a updated version of df_bboxes. The following
                            rows are added:
                            "bill_section" - The section the bbox belongs to
                            "global_mean_delta_x" - The mean x distance of the bboxes of the bill
                            "local_mean_delta_x" - The mean x distance of the corresponding section
                            "upper_ysection_edge" - Normalized upper edge of the corresponding section
                            "lower_ysection_edge" - Normalized lower edge of the corresponding section

    '''
    df_bboxes = df_bboxes.sort_values(by=['y']).reset_index(drop=True)
    columns = ['bill_section', 'global_mean_delta_x', 'local_mean_delta_x', 'upper_ysection_edge', 'lower_ysection_edge']
    for c in columns:
        df_bboxes[c] = 0

    section_edges = area_ysegmentation(df_bboxes, sigma=0)
    global_mean_delta_x = get_mean_xdelta(df_bboxes, xeps=xeps, yeps=yeps)

    number_of_section = len(section_edges)
    segmentation = []
    for section_index in range(1, number_of_section):
        ymin_hline = section_edges[section_index - 1]
        ymax_hline = section_edges[section_index]

        ymin_vline = 1 - ymin_hline / height_image_warped
        ymax_vline = 1 - ymax_hline / height_image_warped

        mask_in_section = (df_bboxes['y'].values <= section_edges[section_index]) & (df_bboxes['y'].values >= section_edges[section_index-1])

        df_bboxes_in = df_bboxes[mask_in_section]
        df_bboxes_in = df_bboxes_in.sort_values(by=['y'])

        local_mean_delta_x = get_mean_xdelta(df_bboxes_in, xeps=xeps, yeps=yeps)
        df_bboxes_in['section_columns'] = segment_section_columns(df_bboxes_in,
                                                                  local_mean_delta_x,
                                                                  global_mean_delta_x)

        df_bboxes_in['bill_section'] = section_index
        df_bboxes_in['global_mean_delta_x'] = global_mean_delta_x
        df_bboxes_in['local_mean_delta_x'] = local_mean_delta_x
        df_bboxes_in['upper_ysection_edge'] = ymin_hline
        df_bboxes_in['lower_ysection_edge'] = ymax_hline
        df_bboxes[mask_in_section] = df_bboxes_in

        if local_mean_delta_x >= global_mean_delta_x:
            vcut = {'delta_x': local_mean_delta_x,
                    'ymin_vline': ymin_vline,
                    'ymax_vline': ymax_vline}

            local_area_segmentation = {'section_index': section_index,
                                       'hcut': section_edges[section_index],
                                       'vcut': vcut}
        else:
            local_area_segmentation = {'section_index': section_index,
                                       'hcut': section_edges[section_index],
                                       'vcut': None}

        segmentation.append(local_area_segmentation)

    return segmentation, df_bboxes
