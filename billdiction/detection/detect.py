import os

import pandas as pd
import cv2 as cv
import matplotlib.pyplot as plt

from pathlib import Path

# BillDiction Library
from billdiction.io import utils
from billdiction.io.data_manipulation import sort_corners
from billdiction.io.dataset import get_dataset
from billdiction.evaluation.plotting import visualize_detection_pipeline, display_roi_text
from billdiction.detection import shape, segment, tesseract, classification


def get_detection_pipeline(image, cornerNN, corner_transforms, threads):
    """
    Infer objects on receipts

    :param image: numpy array representing the image
    :param cornerNN: pytorch model representing a corner prediction network
    :param corner_transforms: processing transformations required by cornerNN
    :param threads: number of threads for tesseract inference
    :return: dictionary with the detection results
    """

    # build the transformation pipeline
    detection_pipe = dict()
    detection_pipe['images'] = dict()
    detection_pipe['images']['input'] = image

    # detect bill shape
    corners, img_corner_prediction = shape.get_shape_cornerNN(image=image,
                                                              model=cornerNN,
                                                              transforms=corner_transforms)
    detection_pipe['images']['corner_prediction'] = img_corner_prediction
    detection_pipe['corners'] = corners

    # transform perspective
    image_warped = utils.warp_perspective(image=image,
                                          corners=sort_corners(corners.squeeze().T),
                                          d=100)
    detection_pipe['images']['top-down-view'] = image_warped

    # Segmentation
    img_segmentation, bboxes, characters = segment.segment_characters(image_warped,
                                                                      rescale_width=400,
                                                                      xdist=10, ydist=0)
    detection_pipe['images']['segmentation'] = img_segmentation
    detection_pipe['characters'] = characters
    detections = detections_dataframe(bboxes)

    detection_pipe['area_edges'], detections = segment.area_segmentation(detections,
                                                                         image_warped.shape[0])
    bboxes = detections.loc[:, 'x':'h'].values
    # Optical character recognition
    detection_pipe['roi_text'], detection_pipe['roi'] = tesseract.tesseract_character_readout(image=image_warped,
                                                                                              bboxes=bboxes,
                                                                                              rescale_width=0,
                                                                                              threads=threads)

    detections["text"] = detection_pipe['roi_text']

    # barcode detector
    barcode_box = segment.segment_barcode(characters, width=image_warped.shape[1])
    classification.label_barcode(barcode_box, detections)

    # detect numerical text fields
    detections = classification.classify(detections, width=image_warped.shape[1])

    # write final result of pipeline
    detection_pipe['detections'] = detections

    return detection_pipe


def detections_dataframe(bboxes):
    """
    Creates a dataframe with gradually stores labels and additional information for all detections

    :param bboxes: (N, 4) array of detection candidates
    :return: dataframe to manage detections later on
    """
    detections = []
    for box in bboxes:
        row = [*box, "entity"]
        detections.append(row)

    columns = ["x", "y", "w", "h", "label"]
    detections = pd.DataFrame(data=detections, columns=columns)

    return detections


def extract_bill_visualization(show_roi_text, cornerNN_path, threads):
    path_to_data = os.path.join(Path(os.path.abspath(__file__)).parents[2],
                                'bills')
    test_data = get_dataset(path_to_data,
                            usage='val',
                            transforms=None,
                            use_rgb=False,
                            training_type='corners')

    cornerNN, corner_transforms = shape.load_cornerNN(cornerNN_path)

    for (stack_number, fn) in zip(test_data.data.stack_number, test_data.data.filename):
        print(stack_number)
        img_path = os.path.join(path_to_data, "img_raw",
                                f'Stack_{stack_number}', fn)

        image = cv.imread(img_path, cv.IMREAD_GRAYSCALE)

        # Detection pipeline
        detection_pipe = get_detection_pipeline(image, cornerNN, corner_transforms, threads=threads)
        fig = visualize_detection_pipeline(detection_pipe)

        plt.show()

        # Displaying the text inside a region of interest
        if show_roi_text:
            display_roi_text(detection_pipe)
