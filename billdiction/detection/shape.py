import imutils
import torch

import cv2 as cv
import numpy as np
from PIL import Image

# BillDiction Library
import billdiction.io.data_augmentation as da
from billdiction.net.inference import CornerNN
from billdiction.net.utils import load_config, predict_single_image, get_device


def get_shape_canny(image, rescale_width, threshold1, threshold2):
    """
    Separate a bill from background using the Canny edge detector
    Reference: https://en.wikipedia.org/wiki/Canny_edge_detector

    :param image: array representing the image
    :param rescale_width:
    :param threshold1:
    :param threshold2:
    :return: bounding box in coordinates of the rescaled image
    """

    # rescale image for detection
    ratio = image.shape[1] / rescale_width
    rescale_height = int(image.shape[0] / ratio)

    img_rescaled = cv.resize(image.copy(), (rescale_width, rescale_height))

    img_canny = cv.Canny(img_rescaled.copy(), threshold1, threshold2)

    cnts = cv.findContours(img_canny.copy(), cv.RETR_LIST, cv.CHAIN_APPROX_SIMPLE)
    cnts = imutils.grab_contours(cnts)
    cnts = sorted(cnts, key=cv.contourArea, reverse=True)[:5]

    # loop over the contours
    for c in cnts:
        # approximate the contour
        peri = cv.arcLength(c, True)
        approx = cv.approxPolyDP(c, 0.02 * peri, True)

        # if our approximated contour has four points, then we
        # can assume that we have found our screen
        if len(approx) == 4:
            screenCnt = approx
            break

    rect = cv.minAreaRect(c)
    box = cv.boxPoints(rect)

    return box * ratio, img_canny, cnts


def load_cornerNN(model_path):
    # TODO: refactor visualization pipe and this one here, so that no code is duplicated
    # read model's training config
    training_config = load_config(model_path)

    # collect relevant variables from parameters
    architecture = training_config['architecture']
    crop_size = training_config['data_augmentation']['crop']['size']

    # load model
    net = CornerNN(model_path)

    transforms = da.ComposeLabel([
        da.ResizeLabel(size=crop_size),
        da.ToTensorLabel(),
        da.NormalizeLabel(mean=training_config['normalization']['mean'],
                          std=training_config['normalization']['std'])
    ])

    return net, transforms


def get_shape_cornerNN(image, model, transforms):
    """
    Detect bills corners using a convolutional neural network

    :param image: array representing the image
    :param model: a pytorch model
    :param transforms: torchvision like transformation to process image before inference (see model config)
    :return: four corners
    """
    labels = torch.zeros(8)

    rescaled_img = Image.fromarray(image)
    rescaled_img, labels = transforms(rescaled_img, labels)

    device = get_device()
    rescaled_img = rescaled_img.to(device, dtype=torch.float32)

    with torch.no_grad():
        bbox = predict_single_image(model, rescaled_img)
        bbox[:, 0] *= image.shape[1] / 240
        bbox[:, 1] *= image.shape[0] / 480

    return bbox, rescaled_img.cpu().numpy()


def warp_perspective(image, corners, d):
    """
    Transforms an image to top-down perspective given a bills corners

    :param image: array representing the image
    :param corners: (4, 2)-shaped array representing the corners
    :param d: margin around the bill in pixel
    :return: top-down view of input image
    """

    width = image.shape[1]
    height = image.shape[0]

    # new coordinates of the corners
    target_points = np.float32([[d, d], [width - d, d], [d, height - d], [width - d, height - d]])

    # perspective transformation
    M = cv.getPerspectiveTransform(corners, target_points)
    img_warped = cv.warpPerspective(src=image, M=M, dsize=(width, height))

    return img_warped
