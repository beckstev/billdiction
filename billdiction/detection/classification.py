import numpy as np

from billdiction.detection.language_processing import get_word_similarity
from billdiction.detection.utils import calc_iou


def drop_bboxes_by_iou(bboxes, reference, iou=0.5):

    mask = np.ones(len(bboxes), dtype='bool')

    for i, box in enumerate(bboxes):
        for ref in reference:
            if len(ref) > 0 and calc_iou(box, ref) > iou:
                mask[i] = 0

    return mask


def associate_lines(detections, eps):
    """

    :param detections: dataframe to manage detections
    :param eps: range where bboxes are assigned new lines (in pixels)
    :return:
    """

    line_counter = 0
    lines = np.zeros(len(detections), dtype=np.uint8)
    last_box = np.zeros(2)

    for i, row in detections.iterrows():
        box_y = row[['y', 'h']].values
        box_y[1] += box_y[0]

        if np.abs(box_y.mean() - last_box.mean()) > eps:
            line_counter += 1

        last_box = box_y.copy()

        lines[i] = line_counter

    detections['line'] = lines

    return detections


def label_barcode(barcode, detections, iou=0.8):
    """
    assigns the barcode label to a bbox with intersection over union > iou

    :param barcode: the bounding box of the barcode candidate
    :param detections: dataframe to manage detections
    :param iou: iou threshold to decide which boxes get associated
    """

    if len(barcode) > 0:
        for i, row in detections.iterrows():
            bbox = row[['x', 'y', 'w', 'h']].values
            if calc_iou(bbox, barcode) > iou:
                detections.loc[i, 'label'] = 'barcode'


def label_logo(detections):
    """
    Assumptions:
        * Logo is the first line

    :param detections: Dataframe to manage detections
    """

    if len(detections) > 0:
        bboxes = detections.loc[:, ['x', 'y', 'w', 'h']].values
        ind = np.argsort(bboxes[:, 1])
        detections.loc[ind[0], 'label'] = 'logo'


def n_numbers(text):
    """
    Gives the number of numerical characters in a string

    :param text: String
    :return: # numerical characters
    """

    numbers = 0
    for char in text:
        if char.isdigit():
            numbers += 1

    return numbers


def get_numerical_regions(regions, texts):
    """
    Gives all numerical text fields

    :param regions: np.array of shape (N, 4) with bounding boxes (x, y, w, h)
    :param texts: list of infered text of the regions
    :return: numerical regions and corresponding texts
    """

    # define functions hyperparameters
    required_numbers = 2

    # regions and texts must be of same size
    assert len(regions) == len(texts), "Regions and corresponding texts must have the same length"

    numbers = [n_numbers(text) for text in texts]
    numbers = np.array(numbers)

    # select numerical text fields
    mask = numbers >= required_numbers
    num_regions = regions[mask]
    num_texts = [text for i, text in enumerate(texts) if mask[i]]

    return num_regions, num_texts


def is_location(text):

    # filter ZIP codes
    is_zip = False
    if len(text) > 9:

        # Feature legend: 0: digit, 1: space, 2: others
        features = np.zeros(len(text))

        for i, char in enumerate(text):
            if char == ' ':
                features[i] = 1
            elif not char.isdigit():
                features[i] = 2

        # filter zip codes
        zip_filter = np.array([0, 0, 0, 0, 0, 1, 2, 2, 2])

        if (features[:9] == zip_filter).all():
            is_zip = True
            print("Location:", text)

    return is_zip

    # TODO: filter address


def is_price(region, text, width):

    is_price = False
    if len(text) > 3:
        # reject large and left aligned regions
        if region[0] < 2 / 3 * width or region[2] > 0.2 * width:
            return False

        # Feature legend: 1: digit, 2: comma or dots, 0: others
        features = np.zeros(len(text))

        for i, char in enumerate(text):
            if char in ['.', ',']:
                features[i] = 2
            elif char.isdigit():
                features[i] = 1

        # filter commata
        comma_filter = np.array([1, 2, 1, 1])
        conv = convolution(features, comma_filter)

        if conv.max() > 6:
            is_price = True

    return is_price


def is_time(text):

    is_time = False

    features = np.zeros(len(text))

    if len(text) >= 5:

        # Feature legend: 1: digit, 2: ':' 3: others
        for i, char in enumerate(text):
            if char in [':', ';']:
                features[i] = 2
            elif char.isdigit():
                features[i] = 1

        # time filter
        time_filter = np.array([1, 1, 2, 1, 1])
        conv = convolution(features, time_filter)

        m = np.argmax(conv)
        if conv[m] == 8:
            print("Time:", text[m:m+5])
            is_time = True

    return is_time


def is_date(text):
    is_date = False
    features = np.zeros(len(text))

    if len(text) >= 7:
        # Feature legend: 1: digit, 2: ':' 0: others
        for i, char in enumerate(text):
            if char in ['.', ',']:
                features[i] = 2
            elif char.isdigit():
                features[i] = 1

        # time filter
        date_filter = np.array([1, 2, 1, 1, 2, 1, 1])
        conv = convolution(features, date_filter)

        m = np.argmax(conv)
        if conv[m] > 10:
            print("Date:", text[m-1:m+9])
            is_date = True

    return is_date


def is_total(region, text, width):
    accept = ['zahlen', 'Zahlen',
              'summe', 'Summe',
              'total', 'Total',
              'gesamt', 'Gesamt']

    is_total = False

    if len(text) > 3:
        # reject large and right aligned regions
        if region[0] > 1 / 3 * width or region[2] > 0.4 * width:
            return False

        spaces = [0]
        for i, c in enumerate(text):
            if c == ' ':
                spaces.append(i)
        spaces.append(len(text))

        for i in range(len(spaces) - 1):
            if i == 0:
                word = text[spaces[i]:spaces[i + 1]]
            else:
                word = text[spaces[i]+1:spaces[i + 1]]

            if word in accept:
                is_total = True

    return is_total


def is_text(text, threshold=0.75):
    alpha = sum([c.isalpha() for c in text])

    if alpha / len(text) >= threshold:
        return True
    else:
        return False


def convolution(features, kernel):
    conv = np.zeros(len(features) - len(kernel) + 1)
    for i in range(len(conv)):
        conv[i] = np.dot(features[i:i + len(kernel)], kernel)
    return conv


def get_label(item, width):
    """
    assigns an item a label
    :param item: row of the detections DataFrame
    :param width: width of the top-down-view image
    :return: label of the item
    """

    total_phrases = ['zahlen', 'summe', 'gesamt', 'total', 'betrag']
    payment_phrases = ['ec', 'bar', 'lastschrift', 'karte', 'ec-karte', 'kartenzahlung']
    change_phrases = ['rückgeld', 'zurück']

    text = item['text']
    box = item[['x', 'y', 'w', 'h']]

    if len(text) == 0:
        label = 'other'
    elif is_location(text):
        label = 'location'
    elif is_time(text):
        label = 'time'
    elif is_date(text):
        label = 'date'
    elif is_price(box, text, width):
        label = 'price'
    elif check_word_class_smiliarity(text, total_phrases, th=0.75):
        label = 'total'
    elif check_word_class_smiliarity(text, payment_phrases, th=0.75):
        label = 'payment'
    elif check_word_class_smiliarity(text, change_phrases, th=0.75):
        label = 'change'
    elif is_text(text, threshold=0.6):
        label = 'text'
    else:
        label = 'other'

    return label


def find_corresponding_price(line, item_index, width):
    """

    :param line:
    :param item_index:
    :param width:
    :return:
    """

    corresponding_index = None

    for i_next_item, next_item in line.iloc[item_index:].iterrows():
        next_item_label = get_label(next_item, width)

        if next_item_label == 'price':
            corresponding_index = i_next_item

    return corresponding_index


def classify(detections, width):
    """
    Classifys each detection in the passed DataFrame

    :param detections: DataFrame to manage detections
    :param width: width of the top-down-view image
    :return: detections DataFrame with labels written to it
    """

    # associates a line number with each detection
    # these line numbers can be used to associate pairs of prices and products
    detections = associate_lines(detections, width * 0.02)
    label_logo(detections)

    # loop through sections of the bill
    detections['corresponding'] = None
    sum_is_detected = False

    for i_section, section in detections.groupby('bill_section'):

        start_line, end_line = section['line'].min(), section['line'].max()

        # loop through the lines of this section
        for i_line in range(start_line, end_line + 1):
            line = section[section['line'] == i_line].sort_values('x')

            # loop through the items in this line
            for i_item in range(len(line)):
                item = line.iloc[i_item]
                label = get_label(item, width)

                # 'text' is the label for product candidates
                if (label == 'text' or label == 'total') and not sum_is_detected:
                    corr_ind = find_corresponding_price(line, i_item, width)

                    if not corr_ind:
                        next_line = section[section['line'] == i_line + 1]
                        start_item_index = 0
                        corr_ind = find_corresponding_price(next_line, start_item_index, width)

                    if label == 'total':
                        sum_is_detected = True
                        if corr_ind:
                            detections.loc[corr_ind, 'label'] = 'sum'
                else:
                    corr_ind = None

                # write labels to detections dataframe
                item_index = item.name
                if detections.loc[item_index, 'label'] == 'entity':
                    detections.loc[item_index, 'label'] = label
                detections.loc[item_index, 'corresponding'] = corr_ind

    mask = detections['label'] == 'other'
    print(detections.loc[mask, 'text'])
    return detections


def predict_number_text(df_bboxes_in, local_mean_delta_x, global_mean_delta_x):
    ''' Function to predict the label of a bbox by using it x-position. Therefore,
        the section has to be vertically separable.
        :param df_bboxes_in: Bboxes inside the section
        :param local_mean_delta_x: Mean x distance between bboxes in the section
                                   of the df_bboxes_in
        :param global_mean_delta_x: Mean x distance of the bill
        :return df_bboxes_in: Returns the df_bboxes_in with modified labels if
                              the local x distance is greater as the global one.
    '''
    if local_mean_delta_x > global_mean_delta_x:
        x = df_bboxes_in['x'].values
        mask = x > local_mean_delta_x
        df_bboxes_in.loc[mask, 'label'] = 'number'
        df_bboxes_in.loc[~mask, 'label'] = 'text'
    return df_bboxes_in


def check_word_class_smiliarity(word, word_phrases, th=0.75):
    if len(word) is 0:
        return False
    word = word.lower()
    words = word.split()
    p_words = []
    for w in words:
        p_w = [get_word_similarity(w, phrase) for phrase in word_phrases]
        p_words += p_w

    p_max = max(p_words)

    if p_max >= th:
        return True
    else:
        return False
