import numpy as np
import cv2 as cv


def rescale_image(image, rescale_width=0, rescale_height=0):
    """
    Takes an image and resize parameters to output a resized version for further processing

    :param image: opencv image
    :param rescale_width: width to which the image is rescaled
    :param rescale_height: height to which the image is rescaled
    :return: processed image
    """
    if rescale_width == 0:
        return image

    else:
        if rescale_height == 0:
            ratio = image.shape[1] / rescale_width
            rescale_height = int(image.shape[0] / ratio)

        resized_img = cv.resize(image, dsize=(rescale_width, rescale_height))

        return resized_img


def crop_image(image, bbox, padding=0):
    """
    Selects a region of an image and returns the cropped image

    :param image: grayscale array in opencv format
    :param bbox: (x, y, w, h) array
    :param padding: (int) pixels to be left around the image
    :return: cropped array
    """
    x, y, w, h = bbox.astype(np.int32)

    height, width = image.shape
    x_min, x_max = max(0, x - padding), min(width, x + w + 1 + padding)
    y_min, y_max = max(0, y - padding), min(height, y + h + 1 + padding)

    crop = image[y_min:y_max, x_min:x_max].copy()
    return crop


def calc_iou(a, b):
    """
    Calculates the intersection over union (IoU) for two bounding boxes. Does not depend on the order of inputs.

    :param a: bbox: (x, y, w, h) array
    :param b: bbox: (x, y, w, h) array
    :return: IoU of a and b
    """

    assert (len(a) == 4) and (len(b) == 4), f"Input has to be single bboxes, but got shape {a.shape}, {b.shape}"

    boxA, boxB = a.copy(), b.copy()

    boxA[2:] += boxA[:2]
    boxB[2:] += boxB[:2]

    # determine the (x, y)-coordinates of the intersection rectangle
    xA = max(boxA[0], boxB[0])
    yA = max(boxA[1], boxB[1])
    xB = min(boxA[2], boxB[2])
    yB = min(boxA[3], boxB[3])

    # compute the area of intersection rectangle
    interArea = max(0, xB - xA + 1) * max(0, yB - yA + 1)

    # compute the area of both the prediction and ground-truth
    # rectangles
    boxAArea = (boxA[2] - boxA[0] + 1) * (boxA[3] - boxA[1] + 1)
    boxBArea = (boxB[2] - boxB[0] + 1) * (boxB[3] - boxB[1] + 1)

    # compute the intersection over union by taking the intersection
    # area and dividing it by the sum of prediction + ground-truth
    # areas - the interesection area
    iou = interArea / float(boxAArea + boxBArea - interArea)

    # return the intersection over union value
    return iou


class HierarchyTree:
    """
    This class can be used to exploit hierarchical relations in findContours for character segmentation
    """
    def __init__(self, hierarchy):
        """

        :param hierarchy: opencv hierarchy e.g. from cv.findContours
        """
        self.hierarchy = hierarchy.squeeze()

        # initialize Tree
        self.first_level_idx, self.first_level_leaves = self.get_first_level()

        self.levels = self.get_deep_levels()

    def get_first_level(self):
        parents = self.hierarchy[:, 3]
        mask = np.where(parents < 0, True, False)

        idx = np.arange(len(self.hierarchy))[mask]
        leaves = self.hierarchy[mask]

        return idx, leaves

    def get_next_level(self, level):
        next_level_idx, next_level_leaves = [], []
        for leaf in level:
            idx = leaf[2]
            if idx < 0:
                continue
            child = self.hierarchy[idx]
            next_level_leaves.append(child)
            next_level_idx.append(idx)

            while child[0] >= 0:
                idx = child[0]
                child = self.hierarchy[idx]
                next_level_leaves.append(child)
                next_level_idx.append(idx)

        return next_level_idx, next_level_leaves

    def get_deep_levels(self):
        levels = [{'idx': self.first_level_idx, 'leaves': self.first_level_leaves}]

        next_level_idx, next_level_leaves = self.get_next_level(self.first_level_leaves)
        while len(next_level_idx) > 0:
            levels.append({'idx': next_level_idx, 'leaves': next_level_leaves})
            next_level_idx, next_level_leaves = self.get_next_level(next_level_leaves)

        return levels

    def depth(self):
        return len(self.levels)

    def __len__(self):
        length = 0
        for layer in self.levels:
            length += len(layer['idx'])

        return length


def image_equalizing(image):
    # Convert RGB to HSV, to use the equalization algorithm
    image_hsv = cv.cvtColor(image, cv.COLOR_BGR2HSV)

    # Global histogram equalization is usefull if the whole image is to dark or
    # bright. However, if the contrast depends on the region a adaptive
    # equalization is a handy and better alternative
    # https://docs.opencv.org/4.0.1/d5/daf/tutorial_py_histogram_equalization.html

    # To apply a adaptive equalization createCLAHE is used. The clipLimit
    # limits the contrast of a region. (Noise reduction). tileGridSize defines
    # the number of rectangles in which a normal equalization is performed
    clahe = cv.createCLAHE(clipLimit=3, tileGridSize=(8, 8))

    # The equalization is applied to the brightness channel of the image
    image_hsv[..., 2] = clahe.apply(image_hsv[..., 2])

    #convert the image back to RGB
    image_thres = image_hsv
    image_thres[..., 2] = cv.adaptiveThreshold(src=image[..., 2],
                                                maxValue=255,
                                                adaptiveMethod=cv.ADAPTIVE_THRESH_MEAN_C,
                                                thresholdType=cv.THRESH_BINARY_INV,
                                                blockSize=15,
                                                C=13)

    image_rgb = cv.cvtColor(image_thres, cv.COLOR_HSV2BGR)

    return image_rgb
