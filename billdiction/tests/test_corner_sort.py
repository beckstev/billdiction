import numpy as np
import os

import matplotlib.pyplot as plt

from torch.utils.data import DataLoader
from pathlib import Path

# BillDiction Library
from billdiction.io.dataset import BillDictionDataset

# TODO: pass configuration file (network architecture) to the script

def test_corner_sort():
    path_to_data = os.path.join(Path(os.path.abspath(__file__)).parents[2], "bills")
    dataset = BillDictionDataset(path_to_data, usage='test')
    data_loader = DataLoader(dataset, batch_size=1, shuffle=False, num_workers=2)  # later on we can perform the test on every single image using the dataloader

    for item in data_loader:
        image, label = item

        fig, ax = plt.subplots(figsize=(5, 10))

        # plot ground truth
        label = label.view(4, -1).numpy()

        x_true = label[:, 0] * dataset.image_width
        y_true = label[:, 1] * dataset.image_height
        print("Ground Truth:\n", label)
        ax.plot(x_true, y_true, 'gx', markersize=10, markeredgewidth=2)

        # test label sorting
        for i in range(4):
            ax.text(x_true[i], y_true[i], str(i), color="purple", fontsize=20)

        # plot the image
        image = image.numpy().reshape(dataset.image_height, dataset.image_width)
        ax.imshow(image, cmap="binary")

        plt.show()

if __name__ == "__main__":
    test_corner_sort()
