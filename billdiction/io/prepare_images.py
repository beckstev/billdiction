import cv2 as cv
import numpy as np


class Pipeline(object):
    def __init__(self, transforms):
        self.transforms = transforms

    def __call__(self, input):
        output = input.copy()
        for t in self.transforms:
            output = t(output)
        return output


class Transformation(object):
    def __init__(self):
        pass

    def __call__(self, image):
        pass


class Kernel(Transformation):
    def __init__(self, method, kernel_size):
        super().__init__()

        self.method = getattr(cv, method)
        self.kernel_size = kernel_size

    def __call__(self, image):
        return self.method(image, self.kernel_size)



class Thresholding(Transformation):
    def __init__(self, method, **kwargs):
        super().__init__()

        self.method = getattr(cv, method)
        self.kwargs = kwargs

    def __call__(self, image):
        return self.method(image, **self.kwargs)


def prepare_corner_prediction(img, gray_scale):
    if not gray_scale:
        img = cv.cvtColor(img, cv.COLOR_RGB2GRAY)

    # smoothing
    sm_size = int(img.shape[0] / 50)
    img = cv.medianBlur(img, sm_size)

    # histogram equalization
    # clahe = cv.createCLAHE(clipLimit=2.0, tileGridSize=(8, 8))
    # img = clahe.apply(img)
    # img = cv.equalizeHist(img)

    # dilation
    dk_size = int(img.shape[0] / 20)
    dilation_kernel = np.ones((dk_size, dk_size), dtype=np.uint(8))
    img = cv.dilate(img, dilation_kernel)

    # thresholding
    #ret, img = cv.threshold(img, 0, 255, cv.THRESH_BINARY+cv.THRESH_OTSU)

    img = cv.adaptiveThreshold(img, 255, cv.ADAPTIVE_THRESH_GAUSSIAN_C, cv.THRESH_BINARY, 11, 2)

    #img = cv.Laplacian(img, cv.CV_8UC1, ksize=5)

    # erosion
    ek_size = int(img.shape[0] / 100)
    erosion_kernel = np.ones((ek_size, ek_size), dtype=np.uint(8))
    img = cv.erode(img, erosion_kernel)

    return img
