import os

import numpy as np
import cv2 as cv

from PIL import ExifTags


def warp_perspective(image, corners, d):
    """
    Transforms an image to top-down perspective given a bills corners

    :param image: array representing the image
    :param corners: (4, 2)-shaped array representing the corners
    :param d: margin around the bill in pixel
    :return: top-down view of input image
    """

    width = int(2*d + np.max([corners[1, 0] - corners[0, 0], corners[3, 0] - corners[2, 0]]))
    height = int(2*d + np.max([corners[2, 1] - corners[0, 1], corners[3, 1] - corners[1, 1]]))

    # new coordinates of the corners
    target_points = np.array([[d, d], [width - d, d], [d, height - d], [width - d, height - d]])

    # perspective transformation
    M = cv.getPerspectiveTransform(corners.astype(np.float32), target_points.astype(np.float32))
    img_warped = cv.warpPerspective(src=image, M=M, dsize=(width, height))

    return img_warped


def get_image_name_dictionary(img_raw_path):
    img_dict = dict()
    for subdir in os.listdir(img_raw_path):
        subdirectory = os.path.join(img_raw_path, subdir)
        if os.path.isdir(subdirectory):
            file_list = os.listdir(subdirectory)
            img_dict[subdir] = [file for file in file_list if '.jpg' in file]
    return img_dict


def sort_corners(corners):
    # first sort upper lower
    index_upper = np.argsort(corners[1])
    corners = corners[..., index_upper]

    # second sort left right
    index_x_upper = np.argsort(corners[0][:2])
    index_x_lower = np.argsort(corners[0][2:])

    # +2 because the positions of the lower corners are
    # at index position 2 and 3
    index_x = np.append(index_x_upper, index_x_lower + 2)

    # sorted corners
    corners = corners.T[index_x]
    return corners


def rotate_exif_image(pil_image):
    try:
        for orientation in ExifTags.TAGS.keys():
            if ExifTags.TAGS[orientation] == 'Orientation':
                break  # grabs the "Orientation" tag from the ExifTags collection and later uses it for testing the orientation

        exif = dict(pil_image._getexif().items())

        if exif[orientation] == 3:
            return pil_image.rotate(180, expand=True), 180
        elif exif[orientation] == 6:
            return pil_image.rotate(270, expand=True), 270
        elif exif[orientation] == 8:
            return pil_image.rotate(90, expand=True), 90

    except:
        return pil_image, 0
