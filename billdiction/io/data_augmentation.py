import numpy as np
import PIL

# PyTorch Library
import torch
import torchvision.transforms.functional as F
from torchvision.transforms import Normalize


class ComposeLabel(object):
    """
    Composes several transformations together while maintaining the labels

    Args:
        transforms (list of ``Transform`` objects): list of transforms to compose.
    """

    def __init__(self, transforms):
        self.transforms = transforms

    def __call__(self, image, label):
        for t in self.transforms:
            image, label = t(image, label)
        return image, label


class ToTensorLabel(object):
    """Convert a ``PIL Image`` or ``numpy.ndarray`` to tensor.

    Converts a PIL Image or numpy.ndarray (H x W x C) in the range
    [0, 255] to a torch.FloatTensor of shape (C x H x W) in the range [0.0, 1.0]
    if the PIL Image belongs to one of the modes (L, LA, P, I, F, RGB, YCbCr, RGBA, CMYK, 1)
    or if the numpy.ndarray has dtype = np.uint8

    In the other cases, tensors are returned without scaling.
    """

    def __call__(self, image, label):
        return F.to_tensor(image), label


class ResizeLabel(object):
    """
    Resizes a PIL image and adjusts labels accordingly

    Args:
        size (tuple): Target size
    """

    def __init__(self, size):
        self.size = size

    def __call__(self, image, label):
        img_size = image.size
        image = image.resize(self.size)

        label[0::2] *= self.size[0] / img_size[0]
        label[1::2] *= self.size[1] / img_size[1]

        return image, label

class NormalizeLabel(object):
    """
    Normalize a tensor image with mean and standard deviation.
    Given mean: ``(M1,...,Mn)`` and std: ``(S1,..,Sn)`` for ``n`` channels, this transform
    will normalize each channel of the input ``torch.*Tensor`` i.e.
    ``input[channel] = (input[channel] - mean[channel]) / std[channel]``

    .. note::
        This transform acts out of place, i.e., it does not mutates the input tensor.

    Args:
        mean (sequence): Sequence of means for each channel.
        std (sequence): Sequence of standard deviations for each channel.
    """

    def __init__(self, mean, std):
        self.mean = mean
        self.std = std
        self.normalize = Normalize(mean=mean, std=std)

    def __call__(self, tensor, label):
        return self.normalize(tensor), label


class RandomHorizontalFlipLabel(object):
    """
    Horizontally flips an image and its corresponding labels with a given probability
    """

    def __init__(self, img_width, p=0.5):
        """
        Horizontally flips an image and its corresponding labels with a given probability

        :param img_width: maximum label in x-dimension
        :param p: probability for the flipping
        """
        self.img_width = img_width
        self.p = p

    def __call__(self, tensor, label):
        if np.random.random() < self.p:
            flipped_label = label.copy()
            flipped_label[0::2] = self.img_width - flipped_label[[2, 0, 6, 4]]
            flipped_label[1::2] = flipped_label[[3, 1, 7, 5]]
            return F.hflip(tensor), flipped_label
        else:
            return tensor, label


class LambdaLabel(object):
    """Apply a user-defined lambda as a transform.

    Args:
        lambd_tensor (function): Lambda/function to be used for transforming the tensor.
        lambd_label (function): Lambda/function to be used for transforming the label.
    """

    def __init__(self, lambd_tensor, lambd_label):
        assert callable(lambd_tensor), repr(type(lambd_tensor).__name__) + " object is not callable"
        assert callable(lambd_label), repr(type(lambd_label).__name__) + " object is not callable"

        self.lambd_tensor = lambd_tensor
        self.lambd_label = lambd_label

    def __call__(self, tensor, label):
        return self.lambd_tensor(tensor), self.lambd_label(label)


class RandomRotationLabel(object):
    """
    Rotate a PIL.Image by a (uniform) random angle and adjust label coordinates accordingly

    Args:
        angle_min (float): lower boundary for rotation angle (in degrees)
        angle_max (float): upper boundary for rotation angle (in degrees)
    """

    def __init__(self, angle_min, angle_max):
        assert angle_min <= angle_max, 'angle_min has to be smaller than angle_max'

        self.phi_min = angle_min
        self.phi_max = angle_max

    def __call__(self, image, label):
        # assure right image type
        if isinstance(image, torch.Tensor):
            image = F.to_pil_image(image)

        elif isinstance(image, np.ndarray):
            image = PIL.Image.fromarray(image)

        elif not isinstance(image, PIL.Image.Image) and not isinstance(image, PIL.JpegImagePlugin.JpegImageFile):
            raise TypeError(f'Unsupported data type of image: {type(image)}')

        # assure right label type
        if isinstance(label, torch.Tensor):
            label = label.cpu().numpy()

        elif not isinstance(label, np.ndarray):
            raise TypeError(f'Unsupported data type of label: {type(label)}')

        if self.phi_min == self.phi_max:
            return image, label

        label = label.reshape(4, 2).T

        # Set the upper and lower angle of the random rotation
        # possible values of phi are 0 - 360 degree
        phi = np.random.uniform(self.phi_min, self.phi_max)
        image_tran = F.rotate(image, phi, expand=True)
        label_tran = self.rotate_label(label, phi, image.size)

        # When we want to keep the size of the input size, we have to
        # resize the roated image to the input size. However we also have to
        # transform the labels
        image_res = image_tran.resize((image.size[0], image.size[1]))
        label_res = self.resize_label(label_tran, image.size, image_tran.size)

        # Output image file directly as PIL image to use plt.imshow
        label_res = label_res.flatten(order='F')

        return image_res, label_res

    def rotate_label(self, label, phi, shape_image_orig):
        phi_rad = np.deg2rad(phi)

        c, s = np.cos(phi_rad), np.sin(phi_rad)
        R = np.array([[c, s], [-s, c]])

        # Rotate the coordinates of the labels
        label = R @ label

        # To use the correct linear transformation it is important to
        # know when the starting base vectors given by the size of the images
        # perfom a sign flip. Everytime a sign flips a linear transformation
        # will be performed to keep the starting coordinate system

        virt_vec_x = R @ np.array([[shape_image_orig[0]], [0]])

        # minus because of the invertet defintion of the y-axis
        virt_vec_y = R @ np.array([[0], [-shape_image_orig[1]]])

        if virt_vec_x[0] < 0:
            label[0] += -virt_vec_x[0]

        if virt_vec_x[1] < 0:
            label[1] += -virt_vec_x[1]

        if virt_vec_y[1] > 0:
            label[1] += virt_vec_y[1]

        if virt_vec_y[0] > 0:
            label[0] += virt_vec_y[0]

        return label

    def resize_label(self, label, shape_image_orig, shape_image_tran):
        dow_x = shape_image_orig[0] / shape_image_tran[0]
        dow_y = shape_image_orig[1] / shape_image_tran[1]

        label[0] *= dow_x
        label[1] *= dow_y

        return label


class RandomCropLabel(object):
    """
    Randomly crop an image given a box (left, upper, right, lower)

    Args:
        size (tuple): Size of the cropped image
        margin (int): Defines how far the cropped image boundary is away from corners

    """

    def __init__(self, size, margin=0):
        self.size = size
        self.margin = margin

        self.resizelabel = ResizeLabel(size)

    def __call__(self, image, label):

        if not F._is_pil_image(image):
            raise TypeError(f'pic should be PIL Image, got {type(image)}')

        assert image.size[0] >= self.size[0] and image.size[1] >= self.size[1], \
            f"The cropped image size should not be larger than the image itself: \n" \
                f"image size: {image.size} \n" \
                f"crop size:  {self.size}"

        x, y = label[::2], label[1::2]

        # get upper left x coordinate
        if x.max() - x.min() + 2 * self.margin < self.size[0] and y.max() - y.min() + 2 * self.margin < self.size[1]:
            # it is possible to crop without losing corners
            x_min = x.max() - self.size[0] if x.max() - self.size[0] > 0 else 0
            x_max = x.min() - self.margin if x.min() - self.margin > 0 else 0
            x_max= x_max if x_max < image.size[0] - self.size[0] else image.size[0] - self.size[0]

            x_min, x_max = int(x_min), int(x_max)

            if x_max == x_min:
                x_rand = x_max
            else:
                x_rand = np.random.randint(x_min, x_max)

            y_min = y.max() - self.size[1] if y.max() - self.size[1] > 0 else 0
            y_max = y.min() - self.margin if y.min() - self.margin > 0 else 0
            y_max = y_max if y_max < image.size[1] - self.size[1] else image.size[1] - self.size[1]

            y_min, y_max = int(y_min), int(y_max)

            if y_max == y_min:
                y_rand = y_max
            else:
                y_rand = np.random.randint(y_min, y_max)

            # crop the image
            box = (x_rand, y_rand, x_rand + self.size[0], y_rand + self.size[1])
            image = image.crop(box=box)

            # adjust the labels
            label[0::2] = x - x_rand
            label[1::2] = y - y_rand

        else:
            # cropping would mean to lose a corner
            image, label = self.resizelabel(image, label)

        return image, label
