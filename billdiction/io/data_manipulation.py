import json
import os
import pandas as pd
import sys

from tqdm import tqdm
from PIL import Image
from pathlib import Path
from sklearn.model_selection import train_test_split

# BillDiction library
from billdiction.io.utils import *


def generate_dataset_labelbox(json_file, img_raw_path,
                     test=0.2, val=0.2, train=0.6,
                     exif_rotation=False):
    if test + val + train != 1.0:
        sys.exit("train, test, val have to add to 1. Please try again.")

    img_dict = get_image_name_dictionary(img_raw_path)

    dict_keys = img_dict.keys()

    with open(json_file) as f:
        label_file = json.load(f)

        labels = []
        filenames = []
        for row in tqdm(label_file):
            if isinstance(row["Label"], dict):
                corner_dict = row["Label"]["Corner"][0]["geometry"]
                if len(corner_dict) == 4:
                    x = []
                    y = []
                    # create 4x2 coordinates array, which can be sorted lexically
                    for corner in corner_dict:
                        x.append(corner["x"])
                        y.append(corner["y"])
                    corners = np.array([x, y])

                    # get information about the image
                    filename = row["External ID"]
                    file_exists = False
                    for key in dict_keys:
                        if filename in img_dict[key]:
                            image = Image.open(os.path.join(img_raw_path, key, filename))

                            if exif_rotation:
                                image, angle = rotate_exif_image(image)
                                if angle != 0:
                                    print(f"File '{filename}' rotated by {angle} degrees.")

                            file_exists = True
                            break

                    if not file_exists:
                        print(f"File '{filename}' does not exists on your storage.")
                        continue

                    # sort corners
                    corners = sort_corners(corners)

                    # flatten the list to be saved in pandas DataFrame
                    corners = corners.flatten().tolist()

                    # read the bounding box of the sum
                    try:
                        sum_list = row["Label"]["Summe"][0]["geometry"]
                        sum_x, sum_y = [], []
                        for corner in sum_list:
                            sum_x.append(corner["x"])
                            sum_y.append(corner["y"])
                        sum_x1, sum_x2 = sorted(set(sum_x))
                        sum_y1, sum_y2 = sorted(set(sum_y))

                        corners.extend([sum_x1, sum_x2, sum_y1, sum_y2])

                    except KeyError:
                        corners.extend([0, 0, 0, 0])

                    # save image size
                    corners.extend(image.size)

                    # append information to dataset
                    filenames.append(filename)
                    labels.append(corners)

        columns = ["x1", "y1", "x2", "y2", "x3", "y3", "x4", "y4", "sum_x1", "sum_x2", "sum_y1", "sum_y2",
                   "width", "height"]

        df = pd.DataFrame(data=labels, columns=columns, dtype='uint16')
        df["filename"] = filenames
        print(df.info())

        # define the training set
        np.random.seed(13)  # guarantees the same outcome for every user working with the same labels and images
        rand = np.random.rand(len(df))
        mask_train = rand < train
        mask_test = rand > 1 - test
        mask_val = np.logical_and(rand > train, rand < 1 - test)

        train_df = df[mask_train].reset_index(drop=True)
        test_df = df[mask_test].reset_index(drop=True)
        val_df = df[mask_val].reset_index(drop=True)

        print(f"\n{len(df)} of {len(label_file)} images selected for the dataset.")
        print("Splitting data set as follows:")
        print(f"Train:      {train} ({len(train_df)} samples)")
        print(f"Test:       {test} ({len(test_df)} samples)")
        print(f"Validation: {val} ({len(val_df)} samples)")

        # save DataFrames to HDF Files
        train_save_path = os.path.join(Path(os.path.abspath(__file__)).parents[2], "bills", "train_labels.csv")
        train_df.to_csv(train_save_path, index=False)
        test_save_path = os.path.join(Path(os.path.abspath(__file__)).parents[2], "bills", "test_labels.csv")
        test_df.to_csv(test_save_path, index=False)
        val_save_path = os.path.join(Path(os.path.abspath(__file__)).parents[2], "bills", "val_labels.csv")
        val_df.to_csv(val_save_path, index=False)


def generate_dataset_imglab(stack_raw_path, test=0.2, val=0.2, train=0.6):
    ''' This functions generates the required dataset to train & test our NNs.
        It uses the labeles created with imglab. The stacks should be stored
        with their labels (as .json file) in seperate folders.

    :param stack_raw_path: Path to folder which contains all stacks.
                           Each stack folder has to be namend in the following
                           way: Stack_[stack number]. Further, each stack
                           folder should include the img labels as a .json
                           file, which is named as follows:
                           Stack_[stack number].json.
    :param test: Percentage of the dataset which will be included into
                 the test dataset
    :param val: Percentage of the dataset which will be included into
                the validation dataset
    :param test: Percentage of the dataset which will be included into
                 the training dataset

    :output: .csv file with the following columns:
            "x1", "y1", "x2", "y2", "x3", "y3", "x4", "y4", "width", "height"
    '''

    if test + val + train != 1.0:
        sys.exit("train, test, val have to add to 1. Please try again.")

    # path to folder with image stacks
    # To estimate the total number of stacks, we use the number of
    # the number of stack folders

    stack_dict = get_image_name_dictionary(stack_raw_path)
    stack_keys = stack_dict.keys()
    number_of_stacks = len(stack_keys)

    # To generate a traning, validation and testing get dataset
    # I use an array which includes all the stack_numbers
    # This array will be first splitted into a (training and validation) and
    # a test dataset. Second the (training and validation) dataset will
    # be splitted into a traning and a validation dataset.
    stack_numbers = np.arange(start=1, stop=number_of_stacks+1)

    stacks_train_val, stacks_test = train_test_split(stack_numbers,
                                                     train_size=train+val,
                                                     test_size=test,
                                                     shuffle=True,
                                                     random_state=13)

    # The test and val size is normalized on three elements:
    # train + val + test = 1. To compensate this, we normalize again with:
    # norm = (1 - train + val) / 2
    norm = (1 - (train + val)) / 2
    train_new = train + norm
    val_new = val + norm

    stacks_train, stacks_val = train_test_split(stacks_train_val,
                                                train_size=train_new,
                                                test_size=val_new,
                                                shuffle=True,
                                                random_state=13)
    # To this point we simply decided which stack contains to training,
    # validation and testing dataset. Now we have to generate for each dataset
    #  a .csv file
    print("Splitting data set as follows:")
    datasets = {'train_labels': stacks_train,
                'test_labels': stacks_test,
                'val_labels': stacks_val}

    for name, stacks in datasets.items():
        df = get_label_df_imglab(stack_raw_path, stacks)

        save_path = os.path.join(Path(os.path.abspath(__file__)).parents[2],
                                 "bills", name + ".csv")
        df.to_csv(save_path, index=False)

        print(f'\n\n The dataset {name} contains {df.shape[0]} images. \n \n')


def get_label_df_imglab(stack_raw_path, stack_numbers):
    """ This function uses the given stack numbers to generate the required
        .csv file. Therefore, it reads the .json file in each stack folder and
        picks the essential information - bounding box coordinates, image
        heigth and width. After collecting all information the function creates
        a pandas dataframe and convert this into a .csv.

    :param stack_raw_path: Path to folder which contains all stacks.
                           Each stack folder has to be namend in the following
                           way: Stack_[stack number]. Further, each stack
                           folder should include the img labels as a .json
                           file, which is named as follows:
                           Stack_[stack number].json.
    :param stack_numbers: Array which contains all the stacks which should be
                          included into the dataset

    :return: Pandas.DataFrame with the following columns:
            "x1", "y1", "x2", "y2", "x3", "y3", "x4", "y4", "width", "height", "stack_number" """



    # Due a dataset can include several stacks, we have to readout each
    # stack seperatley
    dfs = []
    for stack in tqdm(stack_numbers):
        path_to_json = os.path.join(stack_raw_path, f'Stack_{int(stack)}', f'Stack_{int(stack)}.json')

        # Opeing .json file
        dfs.append(readout_imglab_json(path_to_json, stack))

    df = pd.concat(dfs)
    return df


def readout_imglab_json(path_to_json, stack_number):
    """
    low-level function to read all information from imglab json label files and return them as a pandas.DataFrame

    :param path_to_json: path to the json file to be read out
    :param stack_number: (int) number of the stack to be read out
    :return: pandas.DataFrame storing information in the following columns:
                "x1", "y1", "x2", "y2", "x3", "y3", "x4", "y4", "width", "height", "stack_number"
    """

    labels = []
    filenames = []
    # essential information
    columns = ["x1", "y1", "x2", "y2", "x3", "y3", "x4", "y4", "width", "height", "stack_number"]

    with open(path_to_json) as f:
        label_file = json.load(f)
        for ann in label_file['annotations']:

            img_id = ann['image_id']
            img_name = label_file['images'][img_id - 1]['file_name']
            img_path = os.path.join(os.path.dirname(path_to_json), img_name)

            # The .json of imglab also contains the images inside the unique
            # directory without any labels. For that reason the folllowing
            # if condition checks if the image is inside the Stack_ folder
            # or not. If not the image has to be inside the unique directory
            # and will not be considered
            if not os.path.isfile(img_path):
                print(f'Image not in stack directory: {img_path}')
                continue
            img_height = label_file['images'][img_id - 1]['height']
            img_width = label_file['images'][img_id - 1]['width']
            bbox = ann['segmentation'][0]

            # Sometimes the imglab .json contains for some reason a
            # second segemnation/bbox with a not required numbers of labels (8).
            # Therefore, we have to skip this images and have to increase
            # the bbox index offset, to recover the overlap with
            # the 'images' and 'annotations' directory.
            if len(bbox) != 8:
                print('<Imglab bug> - the .json does not contain the required numbers of labels - 8 labels.\n',
                      f'We skip the image {img_path[8:]}!   ')
                continue

            # Get coordinates of the bounding boxes.
            x = bbox[0::2]
            y = bbox[1::2]
            corners = np.array([x, y])
            corners = sort_corners(corners)

            # flatten the list to be saved in pandas DataFrame
            img_label = corners.flatten().tolist()
            img_label.extend([img_width, img_height, stack_number])

            filenames.append(img_name)
            labels.append(img_label)

    df = pd.DataFrame(data=labels, columns=columns, dtype='uint16')
    df["filename"] = filenames
    return df


def generate_rescaled_images(height, width,
                             path_to_data=None,
                             use_rgb=False,
                             exif_rotation=False,
                             preprocessing=None):

    data_base_dir = path_to_data if path_to_data else os.path.join(Path(os.path.abspath(__file__)).parents[2], "bills")
    img_raw_dir = os.path.join(data_base_dir, 'img_raw')

    # create a directory for the rescaled images if not already present
    folder_name = 'img_{}x{}_rgb'.format(height, width) if use_rgb else 'img_{}x{}'.format(height, width)
    rescaled_dir = os.path.join(data_base_dir, folder_name)

    # dictionary to store the rotation angle of each image
    rotation_angles = dict()

    if not os.path.isdir(rescaled_dir):
        os.mkdir(rescaled_dir)
    for subdir in os.listdir(img_raw_dir):
        print(f'Current stack: {subdir}')
        subdirectory = os.path.join(img_raw_dir, subdir)
        if os.path.isdir(subdirectory):
            for image_name in tqdm(os.listdir(subdirectory)):
                if '.jpg' in image_name:
                    try:
                        open_image = os.path.join(subdirectory, image_name)

                        # open and rescale the image
                        image = Image.open(open_image)
                        image = image.resize((width, height))

                        if not use_rgb:
                            image = image.convert('L')
                            if preprocessing:
                                image = apply_preprocessing(image, pipeline=preprocessing)

                        if exif_rotation:
                            image, angle = rotate_exif_image(image)
                            rotation_angles[image_name] = angle

                        # save the rescaled image
                        image_save_path = os.path.join(rescaled_dir, image_name)
                        image.save(image_save_path)

                    except IOError:
                        print("Cannot create rescaled image for file {}".format(image_name))

    # save rotation angles as json
    with open(os.path.join(rescaled_dir, 'rotation_angles.json'), 'w') as f:
        json.dump(rotation_angles, f)


def generate_birdeye_images(path_to_data):
    """
    Takes original images and transforms them to bird-eye-view given corner labels

    :param path_to_data: path to original images
    :return: 0
    """

    # set directory paths
    data_base_dir = path_to_data if path_to_data else os.path.join(Path(os.path.abspath(__file__)).parents[2], 'bills', 'img_raw')

    # loop through all images and transform to birdeye view using corner labels
    for stack in os.listdir(path_to_data):
        stack_number = int(stack[-1])

        # create save folder
        save_dir = os.path.join(os.path.dirname(data_base_dir), 'img_birdeye', stack)
        if not os.path.isdir(save_dir):
            os.makedirs(save_dir)

        # get filenames and labels as dataframe
        # except missing files
        path_to_json = os.path.join(data_base_dir, stack, stack + '_unique', stack + '_unique.json')
        try:
            df = readout_imglab_json(path_to_json, stack_number)
        except FileNotFoundError:
            print(f'File {path_to_json} not found')
            continue

        for i, row in df.iterrows():
            corners = row.loc['x1':'y4'].values.reshape(4, 2)
            img_path = os.path.join(os.path.dirname(path_to_json), row['filename'])

            try:
                # warp the image to bird-eye-view
                image = cv.imread(img_path)
                image_warped = warp_perspective(image, corners, d=0)

                # save image
                save_path = os.path.join(save_dir, row['filename'])
                cv.imwrite(save_path, image_warped)

            except IOError:
                print(f"Cannot create bird-eye-image for file {row['filename']}")


def apply_preprocessing(img, pipeline):
    opencvImage = np.array(img)
    return Image.fromarray(pipeline(opencvImage))
