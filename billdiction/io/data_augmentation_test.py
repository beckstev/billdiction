import os
from pathlib import Path
import matplotlib.pyplot as plt

from torchvision.transforms.functional import to_pil_image
from billdiction.io.dataset import CornerDataset
from billdiction.io.data_augmentation import RandomRotationLabel, NormalizeLabel, ComposeLabel, ToTensorLabel, RandomCropLabel


if __name__ == "__main__":
    path_to_data = os.path.join(Path(os.path.abspath(__file__)).parents[2],
                                "bills")
    rgb = True

    # TODO: Test margin parameter
    transforms = RandomCropLabel(size=(200, 400), margin=20)

    test_data = CornerDataset(path_to_data, usage='test',
                              transforms=transforms, use_rgb=rgb)

    for image, label in test_data:
        image = to_pil_image(image)

        x_pos_of_label = label[::2].numpy()
        y_pos_of_label = label[1::2].numpy()

        plt.imshow(image)

        plt.plot(x_pos_of_label, y_pos_of_label, '.')
        plt.show()
        plt.clf()
