import numpy as np
import torch
import pandas as pd
import os
import sys

from PIL import Image

# PyTorch Library
from torch.utils.data import Dataset
from torchvision.transforms.functional import to_tensor


class BillDictionDataset(Dataset):
    # set methods
    def _set_image_base_path(self, path_to_data, image_size, use_rgb=False):
        if image_size:
            folder_name = 'img_{}x{}_rgb'.format(*image_size) if use_rgb else 'img_{}x{}'.format(*image_size)
            self.image_base_path = os.path.join(path_to_data, folder_name)
        else:
            self.image_base_path = os.path.join(path_to_data, 'img_raw')

    # constructor
    def __init__(self, path_to_data, image_size=(480, 240), usage='train', transforms=None, use_rgb=False):
        if usage not in ['train', 'test', 'val']:
            sys.exit("Usage has to be either 'train', 'test', or 'val'.")

        self.path_to_data = path_to_data
        self.path_to_labels = os.path.join(path_to_data, '{}_labels.csv'.format(usage))

        self.use_rgb = use_rgb
        self.image_height = image_size[0]
        self.image_width = image_size[1]

        self._set_image_base_path(path_to_data, image_size, use_rgb=self.use_rgb)

        # read the labels and related filenames
        self.data = pd.read_csv(self.path_to_labels)

        self.transforms = transforms if transforms else lambda x: x

    def __len__(self):
        pass

    def __getitem__(self, i):
        pass


class CornerDataset(BillDictionDataset):
    def normalize_label(self, i, label):
        original_width, original_height = self.data.loc[i, 'width'], self.data.loc[i, 'height']
        label[0::2] = label[0::2] / original_width * self.image_width
        label[1::2] = label[1::2] / original_height * self.image_height
        return label

    def __init__(self, path_to_data, image_size=(480, 240), usage='train', transforms=None, use_rgb=False):
        super(CornerDataset, self).__init__(path_to_data, image_size, usage, transforms, use_rgb)

    def __len__(self):
        return len(self.data)

    def __getitem__(self, i):
        image_name = os.path.join(self.image_base_path, self.data.loc[i, 'filename'])
        image_pil = Image.open(image_name)

        label = np.array(self.data.loc[i, "x1":"y4"].values, dtype='float32')
        label_rescaled = self.normalize_label(i, label)

        image, label_rescaled = self.transforms(image_pil, label_rescaled)
        label_rescaled = torch.FloatTensor(label_rescaled.flatten(order='F'))

        if not isinstance(image, torch.Tensor):
            image = to_tensor(image)

        return image, label_rescaled


class SumDataset(BillDictionDataset):
    def normalize_label(self, i, label):
        original_width, original_height = self.data.loc[i, 'width'], self.data.loc[i, 'height']
        label[0:2] = label[0:2] / original_width * self.image_width
        label[2:4] = label[2:4] / original_height * self.image_height
        return label

    def __init__(self, path_to_data, image_size=(480, 240), usage='train', transforms=None, use_rgb=False):
        super(SumDataset, self).__init__(path_to_data, image_size, usage, transforms, use_rgb)

    def __len__(self):
        return len(self.data)

    def __getitem__(self, i):
        image_name = os.path.join(self.image_base_path, self.data.loc[i, 'filename'])
        image = np.array(Image.open(image_name))
        label = np.array(self.data.loc[i, "sum_x1":"sum_y2"].values, dtype='float32')
        label_rescaled = self.normalize_label(i, label)

        # bring tensors into the right shape (C_in, height, width)
        if self.use_rgb:
            image = torch.FloatTensor(np.moveaxis(image, 2, 0))
        else:
            image = torch.FloatTensor(image)[None, ...]
        label_rescaled = torch.FloatTensor(label_rescaled)

        image = self.transforms(image)

        return image, label_rescaled


def get_dataset(path_to_data, usage, transforms, use_rgb, training_type):
    if training_type == 'corners':
        data = CornerDataset(path_to_data, usage=usage, transforms=transforms, use_rgb=use_rgb)

    elif training_type == 'sum':
        data = SumDataset(path_to_data, usage=usage, transforms=transforms, use_rgb=use_rgb)

    else:
        raise NotImplementedError

    return data
