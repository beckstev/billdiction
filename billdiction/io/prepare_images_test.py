import os
from pathlib import Path

import matplotlib.pyplot as plt
import cv2 as cv
import pandas as pd

from matplotlib.patches import Polygon
import billdiction.io.prepare_images as pi


def kernel_size(s, i):
    return int(s / i) if int(s / i) % 2 == 1 else int(s / i) + 1


def main():
    test_set = pd.read_csv(os.path.join(Path(os.path.abspath(__file__)).parents[2], "bills", "test_labels.csv"))

    for file in test_set["filename"]:
        file_path = os.path.join(os.path.join(Path(os.path.abspath(__file__)).parents[2], "bills", "img_480x240"), file)
        img = cv.imread(file_path, cv.IMREAD_GRAYSCALE)

        s = img.shape[0]
        pipe = pi.Pipeline([
            pi.Kernel(method="blur", kernel_size=(5, 5)),

            pi.Thresholding(method="adaptiveThreshold",
                            maxValue=255,
                            adaptiveMethod=cv.ADAPTIVE_THRESH_GAUSSIAN_C,
                            thresholdType=cv.THRESH_BINARY,
                            blockSize=11,
                            C=5),
        ])

        img_proc = pipe(img)

        fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(10, 10))
        ax1.imshow(img, cmap="gray")
        ax2.imshow(img_proc, cmap="gray")

        '''
        rect = cv.minAreaRect(c)
        box = cv.boxPoints(rect)
        bbox = Polygon(box, fill=False, color="g", linewidth=3, closed=True)
        ax1.add_patch(bbox)
        '''
        plt.show()


if __name__ == "__main__":
    main()
