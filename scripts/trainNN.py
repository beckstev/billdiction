import argparse

# BillDiction Library
from billdiction.net.training import train
import billdiction.io.data_augmentation as D

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Usage: python main_analysis <architecure>')
    parser.add_argument('architecture', type=str, help='Class name of the network')
    parser.add_argument('--training_type', type=str, help='Type of training procedure')
    parser.add_argument('-e', '--epochs', type=int, help='Number of epochs to train')
    parser.add_argument('-bs', '--batch_size', type=int)
    parser.add_argument('-lr', '--learning_rate', type=float)
    parser.add_argument('-p', '--early_stopping_patience', type=int)
    parser.add_argument('-d', '--early_stopping_delta', type=float)
    parser.add_argument('-wd', '--weight_decay', type=float)
    parser.add_argument('--use_rgb', action='store_true')

    args = parser.parse_args()

    training_type = 'corners'
    if args.training_type:
        training_type = args.training_type

    n_epochs = int(1e5)
    if args.epochs:
        n_epochs = args.epochs

    learning_rate = 1e-4
    if args.learning_rate:
        learning_rate = args.learning_rate

    early_stopping_patience = 30
    if args.early_stopping_patience:
        early_stopping_patience = args.early_stopping_patience

    early_stopping_delta = 0.1
    if args.early_stopping_delta:
        early_stopping_delta = args.early_stopping_delta

    weight_decay = 0
    if args.weight_decay:
        weight_decay = args.weight_decay

    bs_size = 8
    if args.batch_size:
        bs_size = args.batch_size

    transforms = None
    # transformation parameters
    angle_min, angle_max = -20, 20
    input_size = (240, 480)
    crop_size = (240, 480)
    if args.use_rgb:
        norm_mean, norm_std = [0.485, 0.456, 0.406], [0.229, 0.224, 0.225]
    else:
        norm_mean, norm_std = [0.5], [0.2]

    # transformation definition
    # TODO: parse data augmentation methods --crop --rotate etc.
    transforms_train = D.ComposeLabel([
        # D.RandomRotationLabel(angle_min=angle_min, angle_max=angle_max),
        # D.RandomCropLabel(size=crop_size),
        D.RandomHorizontalFlipLabel(img_width=input_size[0], p=0.5),
        D.ToTensorLabel(),
        D.NormalizeLabel(mean=norm_mean, std=norm_std)
    ])
    transforms_val = D.ComposeLabel([
        D.ResizeLabel(size=crop_size),
        D.ToTensorLabel(),
        D.NormalizeLabel(mean=norm_mean, std=norm_std)
    ])


    training_parameters = {
        'batch_size': bs_size,
        'learning_rate': learning_rate,
        'weight_decay': weight_decay,
        'n_epochs': n_epochs,
        'architecture': args.architecture,
        'training_type': training_type,
        'original_image_size': input_size,
        'transforms_train': transforms_train,
        'transforms_val': transforms_val,
        'use_rgb': args.use_rgb,
        'early_stopping_patience': early_stopping_patience,
        'early_stopping_delta': early_stopping_delta,
        'learning_rate_schedule': {
            'factor': 0.2,
            'patience': 15},
        'normalization': {
            'mean': norm_mean,
            'std': norm_std},
        'data_augmentation': {
            'rotation': {
                'angle_min': angle_min,
                'angle_max': angle_max},
            'crop': {
                'size': crop_size
            }
        }
    }

    train(training_parameters)
