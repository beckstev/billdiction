import argparse

# BillDiction Library
from billdiction.evaluation.plotting import visualize_predictions

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Please pass model name')
    parser.add_argument('model_path', type=str,
                        help='Path to the models save directory')
    parser.add_argument('--save_img', action='store_true')
    parser.add_argument('--show_mean', action='store_true')
    parser.add_argument('--shuffle', action='store_true')

    args = parser.parse_args()
    visualize_predictions(args.model_path,
                          save_images=args.save_img,
                          show_mean=args.show_mean,
                          shuffle=args.shuffle)
