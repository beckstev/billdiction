import argparse

# BillDiction Library
from billdiction.io.data_manipulation import generate_birdeye_images

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Usage: python generate_birdeye_images.py <path-to-raw-images>')
    parser.add_argument('path', type=str, help='path to raw images')

    args = parser.parse_args()

    generate_birdeye_images(args.path)
