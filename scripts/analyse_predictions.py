import argparse
import os
import numpy as np
import matplotlib.pyplot as plt
from pathlib import Path

# PyTorch Library
import torch
from torch.utils.data import DataLoader

# BillDiction Library
from billdiction.net.utils import load_config, predict_on_batch
from billdiction.net.inference import CornerNN
from billdiction.io.dataset import get_dataset
from billdiction.io.data_augmentation import ToTensorLabel, NormalizeLabel, ComposeLabel, ResizeLabel
from billdiction.evaluation.evaluate_qualitative import scatter_distribution, histograms_distance_angle, corner_offset
from billdiction.evaluation.evaluate_quantative import distance_angles_mean_std


def get_corner_positions(net, data_loader):
    x_true, y_true, x_pred, y_pred = [], [], [], []

    with torch.no_grad():
        for batch in data_loader:
            X, y = batch

            # get predictions and labels
            x_true_batch, y_true_batch = y[:, 0::2], y[:, 1::2]
            x_pred_batch, y_pred_batch = predict_on_batch(net=net, images=X)

            x_true.append(x_true_batch)
            y_true.append(y_true_batch)
            x_pred.append(x_pred_batch)
            y_pred.append(y_pred_batch)

    x_true, y_true = np.concatenate(x_true), np.concatenate(y_true)
    x_pred, y_pred = np.concatenate(x_pred), np.concatenate(y_pred)

    return x_true, y_true, x_pred, y_pred


def analyze_predictions(model_path, show_figs):
    training_config = load_config(model_path)

    # collect relevant variables from parameters
    architecture = training_config['architecture']
    use_rgb = training_config['use_rgb']
    training_type = training_config['training_type']
    batch_size = training_config['batch_size']
    crop_size = training_config['data_augmentation']['crop']['size']

    img_save_path = os.path.join(model_path, 'img_eval')
    if not os.path.isdir(img_save_path):
        os.mkdir(img_save_path)

    # load data
    path_to_data = os.path.join(Path(os.path.abspath(__file__)).parents[1], 'bills')

    transforms = ComposeLabel([
        ResizeLabel(size=crop_size),
        ToTensorLabel(),
        NormalizeLabel(mean=training_config['normalization']['mean'], std=training_config['normalization']['std'])
    ])

    test_data = get_dataset(path_to_data=path_to_data,
                            usage='test',
                            transforms=transforms,
                            use_rgb=use_rgb,
                            training_type=training_type)

    test_loader = DataLoader(test_data,
                             batch_size=4,#batch_size,
                             shuffle=False,
                             num_workers=3)

    # load model
    net = CornerNN(model_path)

    x_true, y_true, x_pred, y_pred = get_corner_positions(net=net, data_loader=test_loader)

    # quantitative evaluation
    dist_mean, dist_std, angles_mean, angles_std = distance_angles_mean_std(x_true, y_true, x_pred, y_pred)
    print(f'distance deviation: {dist_mean} +- {dist_std}')
    print(f'angle deviation: {angles_mean} +- {angles_std}')
    quantitative = {
        'distances': {
            'mean': dist_mean,
            'std': dist_std,
        },
        'angles': {
            'mean': angles_mean,
            'std': angles_std,
        }
    }

    # qualitative evaluation
    save_path_scatter_distribution = os.path.join(img_save_path, 'scatter_distribution.png')
    fig = scatter_distribution(x_true, y_true, x_pred, y_pred,
                               save_path=save_path_scatter_distribution,
                               architecture=architecture,
                               quantitative=quantitative)
    if show_figs:
        plt.show(fig)

    save_path_histograms = os.path.join(img_save_path, 'histogram_distance_angle.png')
    fig = histograms_distance_angle(x_true, y_true, x_pred, y_pred,
                                    save_path=save_path_histograms,
                                    architecture=architecture,
                                    quantitative=quantitative)
    if show_figs:
        plt.show(fig)

    save_path_offset = os.path.join(img_save_path, 'corner_offset.png')
    fig = corner_offset(x_true, y_true, x_pred, y_pred,
                        save_path=save_path_offset,
                        architecture=architecture,
                        quantitative=quantitative)
    if show_figs:
        plt.show(fig)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Please pass model name')
    parser.add_argument('model_path', type=str, help='Path to the models save directory')
    parser.add_argument('--show_figs', action='store_true')

    args = parser.parse_args()

    analyze_predictions(model_path=args.model_path, show_figs=args.show_figs)
