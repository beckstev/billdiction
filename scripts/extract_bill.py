import argparse
import sys
import os

# BillDiction Library
from billdiction.detection.detect import extract_bill_visualization

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Please pass model name')
    parser.add_argument('cornerNN', type=str, help='path to the desired CornerNN')
    parser.add_argument('-t', '--threads', type=int, default=None)
    parser.add_argument('--show_roi_text', action='store_true')

    args = parser.parse_args()

    if not os.path.isdir(args.cornerNN):
        sys.exit(f"No model directory found at {args.cornerNN}")

    extract_bill_visualization(args.show_roi_text, args.cornerNN, args.threads)
