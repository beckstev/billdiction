import argparse

# BillDiction Library
from billdiction.io.data_manipulation import generate_dataset_labelbox, generate_dataset_imglab

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Usage: python generate_dataset.py  <raw_images_directory> <labeler>')
    parser.add_argument('img_raw_dir', type=str, help='directory containing the stacks.')
    parser.add_argument('labeler', type=str, help='The .json files depending on the used labeler. Labelbox: lbox, imglab: ilab')
    parser.add_argument('--json_file_name', type=str, help='json file containing the labels, only required for labelbox. Otherwise the .json should be inside the stack directory')
    parser.add_argument('--train', type=float, help='Training set fraction of the whole data set.')
    parser.add_argument('--test', type=float, help='Test set fraction of the whole data set.')
    parser.add_argument('--val', type=float, help='Validation set fraction of the whole data set.')

    args = parser.parse_args()

    train = args.train if args.train else 0.6
    test = args.test if args.test else 0.2
    val = args.val if args.val else 0.2

    if args.labeler == 'lbox':
        generate_dataset(args.json_file_name, args.img_raw_dir, train=train, test=test, val=val)
    elif args.labeler == 'ilab':
        generate_dataset_imglab(args.img_raw_dir, train=train, test=test, val=val)
