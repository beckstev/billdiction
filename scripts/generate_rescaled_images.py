import argparse

import cv2 as cv

# BillDiction Library
import billdiction.io.prepare_images as pi
from billdiction.io.data_manipulation import generate_rescaled_images

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Usage: python generate_rescaled_images.py <height> <width>')
    parser.add_argument('height', type=int, help='height of the rescaled image')
    parser.add_argument('width', type=int, help='width of the rescaled image')
    parser.add_argument('--rgb', action='store_true')
    parser.add_argument('--preprocessing', action='store_true')

    args = parser.parse_args()

    preprocessing_pipeline = None
    if args.preprocessing:
        preprocessing_pipeline = pi.Pipeline([
            pi.Thresholding(method="GaussianBlur", ksize=(5, 5), sigmaX=0),

            pi.Thresholding(method="adaptiveThreshold",
                            maxValue=255,
                            adaptiveMethod=cv.ADAPTIVE_THRESH_GAUSSIAN_C,
                            thresholdType=cv.THRESH_BINARY,
                            blockSize=7,
                            C=5),
            pi.Kernel(method="dilate", kernel_size=(5, 5))
        ])

    generate_rescaled_images(args.height, args.width, use_rgb=args.rgb, preprocessing=preprocessing_pipeline)
