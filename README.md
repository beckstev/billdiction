[​![made-with-python](https://img.shields.io/badge/Made%20with-Python-1f425f.svg)](https://www.python.org/)
[![PyPI status](https://img.shields.io/pypi/status/ansicolortags.svg)](https://pypi.python.org/pypi/ansicolortags/)

[![Ask Me Anything !](https://img.shields.io/badge/Ask%20me-anything-1abc9c.svg)](https://GitHub.com/Naereen/ama)





# Welcome to BillDiction.

The project BillDiction started with the idea of a smart finance tracker. Currently receipts have to be processed by hand to extract their information. Because this is a slow and annoying technique, only a minority of people does this. However, our small survey on the campus of TU Dortmund University showed that the majority is interested in a financial overview. But there is no convenient application available. Mark and I decided to use our knowledge in machine learning to develop a solution -- BillDiction.

# How does it work?

BillDiction applies deep learning and computer vision to characterize entries on receipts. Therefore, we are using `Pytorch`, `tesseract`, and `OpenCv`. 

The internal pipeline, first, detects the corners of a receipt (figure __a__) to perform a bird-eye transformation (figure __b__). Based on the transformed image, we detect printed objects (red boxes in figure __b__). With this information we reconstruct entries of the receipts (red boxes in figure __c__). Finally, we classify those boxes with criteria such as the line number (figure __d__). 

<img src="img_readme/detection_example/pipeline_example.png" alt="Example output of Billdiction" style="zoom:100%;" />



# What does BillDiction classify?

The current prototype can classify the following: 

- The Barcode 
- The date of the purchase
- The logo of the store 
- The location of Location 
- The products and the corresponding price
- The time of the purchase
- The total amount you have to pay
- The payment method

In the future, we would also like to classify individual products such as fruit, vegetable, sweets, etc. Due to the difference in naming between grocery stores, we require more data. With this feature, BillDiction could generate a detailed finance statistics within seconds.

# What is required to run BillDiction?

First of all, BillDiction requires a __working__ python version. We recommend using Anaconda, which you can install by following [this guide](https://docs.anaconda.com/anaconda/install/). Afterward, you have to clone the repo:

```
git clone https://gitlab.com/beckstev/billdiction.git
```
The included `setup.py` will install all the required packages except `Pytorch`.
### Packages
You can install the BillDiction framework locally with:
```
pip install -e .
```


The installation of `Pytorch` is a bit trickier, especially if you want to use your __Nvidia GPU__. YYou find a detailed description of how to install `Pytorch` [here](https://pytorch.org/get-started/locally/). Additionally, we have to install [tesseract,](https://github.com/tesseract-ocr/tesseract/wiki#running-tesseract) which is used to extract text from an image. You can install `tesseract` with:

```
sudo apt update sudo apt install tesseract-ocr
```
And the required python wrapper:  
```
pip install pytesseract
```
You should __pay attention__ to the language `tesseract` is using. Some languages have to be [installed separatley](https://askubuntu.com/a/798492)

### Dataset
Unfortunately, we are not able to provide our over 500 collected receipts. Perhaps, we will upload them to [__kaggle__](https://www.kaggle.com/) in the future. This means you have to **create your own dataset**. But we covered you with a short suggestion on how to label.

1. You can label your receipts with [imglab]( https://github.com/NaturalIntelligence/imglab) an open-source label tool.

2. You can use the `Polygon` function of `imglab` to label corners. __Attention:__ Hit enter to save the polygon. 

3. You should use this template to label your data uniformly:

   <img src="img_readme/how_to_label.png" alt="Recommended labels" style="zoom:100%;" />

4. You have to save the labels as a COCO formatted `*.json` file (supported by imglab). Details about
   the [COCO style](https://github.com/cocodataset/cocoapi/issues/179).

## Now I am ready to train, right?

__Almost.__ Before we can start to train, we have to rescale the images and divide them into training, validation and testing datasets. To do so, run the files in the `script` directory:

```
python generate_dataset.py [-h] [--json_file_name JSON_FILE_NAME]
                           [--train TRAIN] [--test TEST] [--val VAL]
                           img_raw_dir labeler

python generate_rescaled_images.py [-h] [--rgb] [--preprocessing] height width
```

For details about the keywords:

```
python generate_dataset.py --help
python generate_rescaled_images.py --help
```

__Ready.__  We suggest training on a `GPU` because of its time efficencie. From the multiple net architectures we implement, we recommend `MiniNNv3`. Is it simple, and keeps the job done.

Run this files inside the `scripts` directory:
```
python trainNN.py [-h] [--training_type TRAINING_TYPE] [-e EPOCHS]
                  [-bs BATCH_SIZE] [-lr LEARNING_RATE]
                  [-p EARLY_STOPPING_PATIENCE] [-d EARLY_STOPPING_DELTA]
                  [-wd WEIGHT_DECAY] [--use_rgb]
                  architecture
```
You get details of the keywords with:

```
python trainNN.py  --help
```

The parameters of the training procedure and the model itself are stored in the `saved_models` directory, and are marked by the timestamp of the start of the training.

## Is it possible to evaluate the results?

__YES!__ You find some ideas inside the `scripts` directory.

Is it possible to test trained models with:

```shell
python analyse_predictions.py [-h] [--show_figs] model_path
```

<img src="./img_readme/img_eval/evaluation_examples.png" alt="Dis" style="zoom:100%;" />



# Suggestions - Mistakes - Errors

Although we are currently not actively implementing, we are __interested__ in your suggestions. If you find an error or mistake, do __not hesitate__ to open an issue. We will reply and search for a solution. 