import os
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable

height, width = np.genfromtxt('roi_height_width.txt', unpack=True)
save_dir = os.path.dirname(__file__)

fig, (ax0, ax1) = plt.subplots(2, 1, figsize=(16, 9))
ax0.hist(height, bins=np.linspace(0, 0.2, 100))
ax0.set_xlabel('Normalized height')
ax0.set_ylabel('Number of ROI')
ax1.hist(width, bins=np.linspace(0, 0.6, 150))
ax1.set_xlabel('Normalized width')
ax1.set_ylabel('Number of ROI')
plt.savefig(os.path.join(save_dir, './width_hist_height_hist.png'), bbox_inches='tight', pad_inches=0.05)
plt.clf()


fig, (ax0, ax1) = plt.subplots(1, 2, figsize=(16, 9))
im = ax1.hist2d(height, width, bins=50, range=[[0, 0.075], [0, 0.4]])
ax1.set_xlabel('Normalized height')
ax1.set_ylabel('Normalized width')
fig.colorbar(im[3], ax=ax1)

ax0.scatter(height, width, s=1)
ax0.set_xlabel('Normalized height')
ax0.set_ylabel('Normalized width')
plt.savefig(os.path.join(save_dir, './width_heigt_scatter_hist2d.png'), bbox_inches='tight', pad_inches=0.05)
