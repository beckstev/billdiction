import numpy as np
import matplotlib.pyplot as plt
import cv2 as cv
from billdiction.io.dataset import get_dataset
from billdiction.detection import shape, segment
from billdiction.io import utils
import os
from pathlib import Path
from tqdm import tqdm
path_to_data = os.path.join(Path(os.path.abspath(__file__)).parents[2],
                            'bills')

test_data = get_dataset(path_to_data,
                        usage='test',
                        transforms=None,
                        use_rgb=False,
                        training_type='corners')
height = np.empty(shape=0)
width = np.empty(shape=0)

for (stack_number, fn) in tqdm(zip(test_data.data.stack_number, test_data.data.filename)):
    img_path = os.path.join(path_to_data, "img_raw",
                            f'Stack_{stack_number}', fn)

    image = cv.imread(img_path, cv.IMREAD_GRAYSCALE)
    _, corners_sorted, _, _ = shape.get_shape_canny(image=image,
                                                    rescale_width=400,
                                                    threshold1=50,
                                                    threshold2=100)

    image_warped = utils.warp_perspective(image=image,
                                          corners=corners_sorted,
                                          d=100)
    img_height = image_warped.shape[0]
    img_width = image_warped.shape[1]

    _, _, regions = segment.segment_pyimagesearch(image_warped, rescale_width=400)

    num_roi = len(regions)

    height_roi = np.empty(shape=num_roi)
    width_roi = np.empty(shape=num_roi)

    for index in range(num_roi):
        height_roi[index] = regions[index].shape[0]
        width_roi[index] = regions[index].shape[1]

    height_roi *= 1/img_width
    width_roi *= 1/img_width

    height = np.concatenate((height, height_roi))
    width = np.concatenate((width, width_roi))

np.savetxt('./roi_height_width.txt', np.column_stack([height, width]), header='Height Width')
