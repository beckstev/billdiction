from setuptools import setup
import os


setup(name='billdiction',
      version='0.1',
      description='Package to extract information from receipts',
      author='Steven Becker, Mark Schoene',
      author_email='steven.becker@tu-dortmund.de, mark.schoene@protonmail.com',
      packages=['billdiction'],
      install_requires=['numpy',
                        'pandas',
                        'matplotlib',
                        'pathlib',
                        'pillow',
                        'tqdm',
                        'h5py'
                        #'torch',
                        #'torchvision'
                        ])
